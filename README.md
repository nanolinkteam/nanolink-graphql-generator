This project contains the tools to autogenerate an API interface for the NanoLink ApS public api.

Steps:
 1. Run the generator program (nanolink.graphql.generator)
 2. Build the api (nanolink.graphql.api)

Now there is assembly ready to use

See the example application on how the api is used. Remember to put in your own API token.