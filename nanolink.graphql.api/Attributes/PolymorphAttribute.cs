﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;

namespace nanolink.graphql.api
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class PolymorphAttribute: Attribute
    {
        public Type[] SubClasses { get; set; }
        public bool UseUnion
        {
            get
            {
                return SubClasses != null && SubClasses.Length > 0;
            }
        }
        public PolymorphAttribute(bool isAbstract, params Type[] subclasses)
        {
            SubClasses = subclasses;
        }
        public PolymorphAttribute(params Type[] subclasses)
        {
            SubClasses = subclasses;
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, Inherited = true)]
    public class GraphQLNameAttribute: Attribute
    {
        public string Name { get; set; }
        public GraphQLNameAttribute(string name) => Name = name;
    }

    public static class GraphQLNameAttributeExtension
    {
        public static T LocateAttribute<T>(this Type t) where T: Attribute
        {
            T retVal = t.GetCustomAttribute<T>();
            if (retVal == null)
            {
                retVal = (from n in t.GetInterfaces() where 
                          n.GetCustomAttribute<T>() != null select n.GetCustomAttribute<T>()).FirstOrDefault();
            }
            return retVal;
        }
        public static Tuple<T,Type> LocateAttributeWithType<T>(this Type t, bool inherit = true) where T : Attribute
        {
            T attr = null;
            Tuple<T, Type> retVal = null;
            if ((attr = t.GetCustomAttribute<T>(inherit)) != null)
                retVal = new Tuple<T, Type>(attr, t);
            if (retVal == null)
            {
                retVal = (from n in t.GetInterfaces()
                          where n.GetCustomAttribute<T>() != null
                          select new Tuple<T,Type>(n.GetCustomAttribute<T>(), n)).FirstOrDefault();
            }
            return retVal;
        }
        public static string GraphQLName(this Type t)
        {
            try
            {
                string name = t.LocateAttribute<GraphQLNameAttribute>()?.Name;
                if (name == null)
                {
                    if (t.Name.StartsWith("Q"))
                        return t.Name;
                    else
                        return $"Q{t.Name}";
                }
                else
                {
                    return name;
                }
            }
            catch
            {
                throw;
            }
        }
        private static ConcurrentDictionary<string, List<Type>> _cacheNameSpaces = new ConcurrentDictionary<string, List<Type>>();
        public static List<Type> NameSpaceTypes(this Assembly assembly, string nameSpace)
        {
            return _cacheNameSpaces.GetOrAdd(nameSpace, (nspc) => (from n in assembly.GetTypes() where n.Namespace == nspc select n).ToList());
        }

        public static IEnumerable<Type> GetPolymorphTypes(this Type type)
        {
            Tuple<PolymorphAttribute, Type> attr = LocateAttributeWithType<PolymorphAttribute>(type, false);
            if (attr != null && attr.Item2 == type)
            {
                foreach (var n in attr.Item1.SubClasses)
                {
                    var res = (from m in NameSpaceTypes(type.Assembly, type.Namespace)
                                where m != type && n.IsAssignableFrom(m) // && curType.IsAssignableFrom(m)
                                select m).FirstOrDefault();
                    if (res != null)
                        yield return res;
                }
            }
        }
    }
}

