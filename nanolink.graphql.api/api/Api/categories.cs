using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
using System.Globalization;
using System.Threading.Tasks;
namespace nanolink.graphql.api
{
    public static class CategoriesApi
    {
        public static async Task<ApiResultList<T>> Categories_Getall<T>(this GraphQLClient con,string token,bool? sort = default,string scale = default) where T: QCategory
        {
            IEnumerable<string> genargs()
            {
                if (sort.HasValue) yield return $"sort: {sort.Value.ToString().ToLower()}";
                if (scale != null) yield return $"scale: {JsonConvert.ToString(scale,'\"', StringEscapeHandling.Default)}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "categories_getall", true, args, null)) as ApiResultList<T>;
        }
        public static async Task<ApiResultList<T>> Categories_Getlevel<T>(this GraphQLClient con,string token,string parent = default,bool? sort = default,string scale = default,bool? includeParent = default) where T: QCategory
        {
            IEnumerable<string> genargs()
            {
                if (parent != null) yield return $"parent: {JsonConvert.ToString(parent,'\"', StringEscapeHandling.Default)}";
                if (sort.HasValue) yield return $"sort: {sort.Value.ToString().ToLower()}";
                if (scale != null) yield return $"scale: {JsonConvert.ToString(scale,'\"', StringEscapeHandling.Default)}";
                if (includeParent.HasValue) yield return $"includeParent: {includeParent.Value.ToString().ToLower()}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "categories_getlevel", true, args, null)) as ApiResultList<T>;
        }
    }
}
