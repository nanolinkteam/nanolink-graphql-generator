using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
using System.Globalization;
using System.Threading.Tasks;
namespace nanolink.graphql.api
{
    public static class AuthApi
    {
        public static async Task<ApiResult<string>> Auth_Externallogin(this GraphQLClient con,string logintoken = default)
        {
            IEnumerable<string> genargs()
            {
                if (logintoken != null) yield return $"logintoken: {JsonConvert.ToString(logintoken,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<string>("api/public", false,  "auth_externallogin", false, args, null)) as ApiResult<string>;
        }
    }
}
