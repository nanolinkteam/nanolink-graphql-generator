using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
using System.Globalization;
using System.Threading.Tasks;
namespace nanolink.graphql.api
{
    public static class LinksApi
    {
        public static async Task<ApiResultList<T>> Links_List_Foundby_Asset<T>(this GraphQLClient con,string token,ObjectId refId,int? timeout = default) where T: QReference
        {
            IEnumerable<string> genargs()
            {
                yield return $"refId: \"{refId.ToString()}\"";
                if (timeout.HasValue) yield return $"timeout: {timeout.Value}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "links_list_foundby_asset", true, args, null)) as ApiResultList<T>;
        }
        public static async Task<ApiResultList<T>> Links_List_Foundby_Location<T>(this GraphQLClient con,string token,ObjectId refId,int? timeout = default) where T: QReference
        {
            IEnumerable<string> genargs()
            {
                yield return $"refId: \"{refId.ToString()}\"";
                if (timeout.HasValue) yield return $"timeout: {timeout.Value}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "links_list_foundby_location", true, args, null)) as ApiResultList<T>;
        }
        public static async Task<ApiResultList<T>> Links_List_Foundby_Person<T>(this GraphQLClient con,string token,ObjectId refId,int? timeout = default) where T: QReference
        {
            IEnumerable<string> genargs()
            {
                yield return $"refId: \"{refId.ToString()}\"";
                if (timeout.HasValue) yield return $"timeout: {timeout.Value}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "links_list_foundby_person", true, args, null)) as ApiResultList<T>;
        }
        public static async Task<ApiResultList<T>> Links_List_Markedby_Person<T>(this GraphQLClient con,string token,ObjectId refId,int? timeout = default) where T: QReference
        {
            IEnumerable<string> genargs()
            {
                yield return $"refId: \"{refId.ToString()}\"";
                if (timeout.HasValue) yield return $"timeout: {timeout.Value}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "links_list_markedby_person", true, args, null)) as ApiResultList<T>;
        }
    }
}
