using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
using System.Globalization;
using System.Threading.Tasks;
namespace nanolink.graphql.api
{
    public static class ReferencesApi
    {
        public static async Task<ApiResult<T>> References_Getlocation<T>(this GraphQLClient con,string token,ObjectId id) where T: QLocation
        {
            IEnumerable<string> genargs()
            {
                yield return $"id: \"{id.ToString()}\"";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "references_getlocation", false, args, null)) as ApiResult<T>;
        }
        public static async Task<ApiResult<T>> References_Getuser<T>(this GraphQLClient con,string token,ObjectId id) where T: QUser
        {
            IEnumerable<string> genargs()
            {
                yield return $"id: \"{id.ToString()}\"";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "references_getuser", false, args, null)) as ApiResult<T>;
        }
        public static async Task<ApiResultList<T>> References_Searchlocation<T>(this GraphQLClient con,string token,int? limit = default,int? page = default,List<PSortKey> sort = default,string search = default) where T: QLocation
        {
            IEnumerable<string> genargs()
            {
                if (limit.HasValue) yield return $"limit: {limit.Value}";
                if (page.HasValue) yield return $"page: {page.Value}";
                if (sort != null) yield return $"sort: {sort.ToGraphQLJson()}";
                if (search != null) yield return $"search: {JsonConvert.ToString(search,'\"', StringEscapeHandling.Default)}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "references_searchlocation", true, args, null)) as ApiResultList<T>;
        }
        public static async Task<ApiResultList<T>> References_Searchuser<T>(this GraphQLClient con,string token,int? limit = default,int? page = default,List<PSortKey> sort = default,string search = default,List<string> groupIds = default) where T: QUser
        {
            IEnumerable<string> genargs()
            {
                if (limit.HasValue) yield return $"limit: {limit.Value}";
                if (page.HasValue) yield return $"page: {page.Value}";
                if (sort != null) yield return $"sort: {sort.ToGraphQLJson()}";
                if (search != null) yield return $"search: {JsonConvert.ToString(search,'\"', StringEscapeHandling.Default)}";
                if (groupIds != null) yield return $"groupIds: {groupIds.ToGraphQLJson()}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "references_searchuser", true, args, null)) as ApiResultList<T>;
        }
        public static async Task<ApiResult<bool>> References_Deletelocation(this GraphQLClient con,string token,ObjectId id,int version)
        {
            IEnumerable<string> genargs()
            {
                yield return $"id: \"{id.ToString()}\"";
                yield return $"version: {version}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<bool>("api/public", true, "references_deletelocation", false, args, null)) as ApiResult<bool>;
        }
        public static async Task<ApiResult<bool>> References_Deleteuser(this GraphQLClient con,string token,ObjectId id,int version)
        {
            IEnumerable<string> genargs()
            {
                yield return $"id: \"{id.ToString()}\"";
                yield return $"version: {version}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<bool>("api/public", true, "references_deleteuser", false, args, null)) as ApiResult<bool>;
        }
        public static async Task<ApiResult<bool>> References_Passwordreset(this GraphQLClient con,string token,ObjectId userId,string lang = default)
        {
            IEnumerable<string> genargs()
            {
                yield return $"userId: \"{userId.ToString()}\"";
                if (lang != null) yield return $"lang: {JsonConvert.ToString(lang,'\"', StringEscapeHandling.Default)}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<bool>("api/public", true, "references_passwordreset", false, args, null)) as ApiResult<bool>;
        }
        public static async Task<ApiResult<string>> References_Passwordreseturl(this GraphQLClient con,string token,ObjectId userId)
        {
            IEnumerable<string> genargs()
            {
                yield return $"userId: \"{userId.ToString()}\"";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<string>("api/public", true, "references_passwordreseturl", false, args, null)) as ApiResult<string>;
        }
        public static async Task<ApiResult<T>> References_Savelocation<T>(this GraphQLClient con,string token,PLocationMutation update = default) where T: QLocation
        {
            IEnumerable<string> genargs()
            {
                if (update != null) yield return $"update: {update.ToGraphQLJson()}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", true, "references_savelocation", false, args, null)) as ApiResult<T>;
        }
        public static async Task<ApiResult<T>> References_Saveuser<T>(this GraphQLClient con,string token,PUserMutation update = default,bool? sendVerificationEmail = default,bool? sendWelcomeSMS = default) where T: QUser
        {
            IEnumerable<string> genargs()
            {
                if (update != null) yield return $"update: {update.ToGraphQLJson()}";
                if (sendVerificationEmail.HasValue) yield return $"sendVerificationEmail: {sendVerificationEmail.Value.ToString().ToLower()}";
                if (sendWelcomeSMS.HasValue) yield return $"sendWelcomeSMS: {sendWelcomeSMS.Value.ToString().ToLower()}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", true, "references_saveuser", false, args, null)) as ApiResult<T>;
        }
        public static async Task<ApiResult<bool>> References_Welcomesms(this GraphQLClient con,string token,ObjectId userId,string lang = default)
        {
            IEnumerable<string> genargs()
            {
                yield return $"userId: \"{userId.ToString()}\"";
                if (lang != null) yield return $"lang: {JsonConvert.ToString(lang,'\"', StringEscapeHandling.Default)}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<bool>("api/public", true, "references_welcomesms", false, args, null)) as ApiResult<bool>;
        }
    }
}
