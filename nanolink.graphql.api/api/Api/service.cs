using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
using System.Globalization;
using System.Threading.Tasks;
namespace nanolink.graphql.api
{
    public static class ServiceApi
    {
        public static async Task<ApiResult<T>> Service_Get<T>(this GraphQLClient con,string token,ObjectId id) where T: QServicePlan
        {
            IEnumerable<string> genargs()
            {
                yield return $"id: \"{id.ToString()}\"";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "service_get", false, args, null)) as ApiResult<T>;
        }
        public static async Task<ApiResultList<T>> Service_Search<T>(this GraphQLClient con,string token,int? limit = default,int? page = default,List<PSortKey> sort = default,string search = default) where T: QServicePlan
        {
            IEnumerable<string> genargs()
            {
                if (limit.HasValue) yield return $"limit: {limit.Value}";
                if (page.HasValue) yield return $"page: {page.Value}";
                if (sort != null) yield return $"sort: {sort.ToGraphQLJson()}";
                if (search != null) yield return $"search: {JsonConvert.ToString(search,'\"', StringEscapeHandling.Default)}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "service_search", true, args, null)) as ApiResultList<T>;
        }
    }
}
