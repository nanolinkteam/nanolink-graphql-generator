using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
using System.Globalization;
using System.Threading.Tasks;
namespace nanolink.graphql.api
{
    public static class AssetsApi
    {
        public static async Task<ApiResult<T>> Assets_Get<T>(this GraphQLClient con,string token,ObjectId id,string imgscale = default,int? imgradius = default) where T: QAsset
        {
            IEnumerable<string> genargs()
            {
                yield return $"id: \"{id.ToString()}\"";
                if (imgscale != null) yield return $"imgscale: {JsonConvert.ToString(imgscale,'\"', StringEscapeHandling.Default)}";
                if (imgradius.HasValue) yield return $"imgradius: {imgradius.Value}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "assets_get", false, args, null)) as ApiResult<T>;
        }
        public static async Task<ApiResultList<T>> Assets_Logassets<T>(this GraphQLClient con,string token,DateTime? fromDateTime = default) where T: QAssetRecord
        {
            IEnumerable<string> genargs()
            {
                if (fromDateTime.HasValue) yield return $"fromDateTime: \"{fromDateTime.Value:o}\"";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "assets_logassets", true, args, null)) as ApiResultList<T>;
        }
        public static async Task<ApiResultList<T>> Assets_Loglocations<T>(this GraphQLClient con,string token,DateTime? fromDateTime = default) where T: QAssetRecord
        {
            IEnumerable<string> genargs()
            {
                if (fromDateTime.HasValue) yield return $"fromDateTime: \"{fromDateTime.Value:o}\"";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "assets_loglocations", true, args, null)) as ApiResultList<T>;
        }
        public static async Task<ApiResultList<T>> Assets_Logpersons<T>(this GraphQLClient con,string token,DateTime? fromDateTime = default) where T: QAssetRecord
        {
            IEnumerable<string> genargs()
            {
                if (fromDateTime.HasValue) yield return $"fromDateTime: \"{fromDateTime.Value:o}\"";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "assets_logpersons", true, args, null)) as ApiResultList<T>;
        }
        public static async Task<ApiResultList<T>> Assets_Logposition<T>(this GraphQLClient con,string token,DateTime? fromDateTime = default) where T: QAssetRecord
        {
            IEnumerable<string> genargs()
            {
                if (fromDateTime.HasValue) yield return $"fromDateTime: \"{fromDateTime.Value:o}\"";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "assets_logposition", true, args, null)) as ApiResultList<T>;
        }
        public static async Task<ApiResultList<T>> Assets_Positionchanges<T>(this GraphQLClient con,string token,DateTime? fromDateTime = default) where T: QAsset
        {
            IEnumerable<string> genargs()
            {
                if (fromDateTime.HasValue) yield return $"fromDateTime: \"{fromDateTime.Value:o}\"";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "assets_positionchanges", true, args, null)) as ApiResultList<T>;
        }
        public static async Task<ApiResultList<T>> Assets_Search<T>(this GraphQLClient con,string token,int? limit = default,int? page = default,List<PSortKey> sort = default,string search = default,string imgscale = default,int? imgradius = default,List<string> categoryIds = default,List<string> servicePlanIds = default,List<int> serviceStatus = default,PTagFilter tagFilter = default) where T: QAsset
        {
            IEnumerable<string> genargs()
            {
                if (limit.HasValue) yield return $"limit: {limit.Value}";
                if (page.HasValue) yield return $"page: {page.Value}";
                if (sort != null) yield return $"sort: {sort.ToGraphQLJson()}";
                if (search != null) yield return $"search: {JsonConvert.ToString(search,'\"', StringEscapeHandling.Default)}";
                if (imgscale != null) yield return $"imgscale: {JsonConvert.ToString(imgscale,'\"', StringEscapeHandling.Default)}";
                if (imgradius.HasValue) yield return $"imgradius: {imgradius.Value}";
                if (categoryIds != null) yield return $"categoryIds: {categoryIds.ToGraphQLJson()}";
                if (servicePlanIds != null) yield return $"servicePlanIds: {servicePlanIds.ToGraphQLJson()}";
                if (serviceStatus != null) yield return $"serviceStatus: {serviceStatus.ToGraphQLJson()}";
                if (tagFilter != null) yield return $"tagFilter: {tagFilter.ToGraphQLJson()}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "assets_search", true, args, null)) as ApiResultList<T>;
        }
        public static async Task<ApiResultList<T>> Assets_Unpairednanolinks<T>(this GraphQLClient con,string token,int? limit = default,int? page = default) where T: QNanoLink
        {
            IEnumerable<string> genargs()
            {
                if (limit.HasValue) yield return $"limit: {limit.Value}";
                if (page.HasValue) yield return $"page: {page.Value}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", false,  "assets_unpairednanolinks", true, args, null)) as ApiResultList<T>;
        }
        public static async Task<ApiResult<bool>> Assets_Deleteasset(this GraphQLClient con,string token,ObjectId id,int version)
        {
            IEnumerable<string> genargs()
            {
                yield return $"id: \"{id.ToString()}\"";
                yield return $"version: {version}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<bool>("api/public", true, "assets_deleteasset", false, args, null)) as ApiResult<bool>;
        }
        public static async Task<ApiResult<T>> Assets_Savetoolasset<T>(this GraphQLClient con,string token,PToolAssetMutation update = default,string returnImgScale = default,int? returnImgRadius = default) where T: QAsset
        {
            IEnumerable<string> genargs()
            {
                if (update != null) yield return $"update: {update.ToGraphQLJson()}";
                if (returnImgScale != null) yield return $"returnImgScale: {JsonConvert.ToString(returnImgScale,'\"', StringEscapeHandling.Default)}";
                if (returnImgRadius.HasValue) yield return $"returnImgRadius: {returnImgRadius.Value}";
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<T>("api/public", true, "assets_savetoolasset", false, args, null)) as ApiResult<T>;
        }
    }
}
