using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
using System.Globalization;
using System.Threading.Tasks;
namespace nanolink.graphql.api
{
    public static class StatApi
    {
        public static async Task<ApiResult<int>> Stat_Currentversion(this GraphQLClient con,string token)
        {
            IEnumerable<string> genargs()
            {
                yield return $"token: {JsonConvert.ToString(token,'\"', StringEscapeHandling.Default)}";
            }
            string args = string.Join(",", genargs());
            return (await con.Post<int>("api/public", false,  "stat_currentversion", false, args, null)) as ApiResult<int>;
        }
    }
}
