using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    public enum ServiceLogEventCode
    {
        OVERDUE,
        PENDING,
        COMPLETED
    }
}
