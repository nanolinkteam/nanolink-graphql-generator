using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QGateDevice: QDeviceBase
    {
        public virtual QDetectorInfoGate LastGateInfo { get => default; set {} }
    }
}
