using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QAsset: QReference
    {
        public virtual bool AssetWatch { get => default; set {} }
        public virtual string Brand { get => default; set {} }
        public virtual string Description { get => default; set {} }
        public virtual int DisabledWatches { get => default; set {} }
        public virtual double? Distance { get => default; set {} }
        public virtual int EnabledWatches { get => default; set {} }
        public virtual List<QJobAsset> Jobs { get => default; set {} }
        public virtual string KeyWords { get => default; set {} }
        public virtual string Model { get => default; set {} }
        public virtual string Serial { get => default; set {} }
    }
}
