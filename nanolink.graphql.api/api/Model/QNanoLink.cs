using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QNanoLink
    {
        public virtual DateTime CreatedDateTime { get => default; set {} }
        public virtual string ExternalBag { get => default; set {} }
        public virtual List<string> ExternalKeys { get => default; set {} }
        public virtual string Id { get => default; set {} }
        public virtual bool IsManualTag { get => default; set {} }
        public virtual bool IsTracker { get => default; set {} }
        public virtual QAssetRecord LastKnownAssetRecords { get => default; set {} }
        public virtual QAssetRecord LastKnownAssetRecordsByAny { get => default; set {} }
        public virtual QAssetRecord LastKnownAssetRecordsByAsset { get => default; set {} }
        public virtual QAssetRecord LastKnownAssetRecordsByGate { get => default; set {} }
        public virtual QAssetRecord LastKnownAssetRecordsByManual { get => default; set {} }
        public virtual QAssetRecord LastKnownAssetRecordsByPerson { get => default; set {} }
        public virtual string Mac { get => default; set {} }
        public virtual TagType TagType { get => default; set {} }
        public virtual string VID { get => default; set {} }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
