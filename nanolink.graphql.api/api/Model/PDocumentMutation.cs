using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    public class PDocumentMutation
    {
        public ObjectId? Id { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public string Blob { get; set; }
        public bool Copy { get; set; }
        public string Url { get; set; }
    }
}
