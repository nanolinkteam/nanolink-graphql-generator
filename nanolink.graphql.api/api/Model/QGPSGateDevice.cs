using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QGPSGateDevice: QDeviceBase
    {
        public virtual QDetectorInfoGPSGate LastGPSGateInfo { get => default; set {} }
    }
}
