using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    public class PGPSLocationInfo
    {
        public DateTime Date { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public double Altitude { get; set; }
        public double Speed { get; set; }
        public double Bearing { get; set; }
        public double Accuracy { get; set; }
    }
}
