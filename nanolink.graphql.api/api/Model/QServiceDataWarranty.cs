using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QServiceDataWarranty: QServiceData
    {
        public virtual DateTime PurchaseDate { get => default; set {} }
        public virtual int WarrantyInMonths { get => default; set {} }
    }
}
