using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QKnoxLicenseInfo
    {
        public virtual int LicenseAttestationStatus { get => default; set {} }
        public virtual int LicenseError { get => default; set {} }
        public virtual int LicenseResult { get => default; set {} }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
