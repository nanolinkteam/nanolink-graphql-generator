using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QServicePlanWarranty: QServicePlan
    {
        public virtual int WarrantyInMonths { get => default; set {} }
    }
}
