using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QUser: QReference
    {
        public virtual string Email { get => default; set {} }
        public virtual string FirstName { get => default; set {} }
        public virtual string FullName { get => default; set {} }
        public virtual string LastName { get => default; set {} }
        public virtual string MiddleName { get => default; set {} }
        public virtual string MobileCountryCode { get => default; set {} }
        public virtual string MobilePhoneNumber { get => default; set {} }
    }
}
