using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QMobileDevice: QDeviceBase
    {
        public virtual QDetectorInfoMobile LastMobileInfo { get => default; set {} }
    }
}
