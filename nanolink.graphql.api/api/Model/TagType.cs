using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    public enum TagType
    {
        MANUAL,
        BEACON,
        MOBILE,
        GATE,
        TRACKER,
        COMPUTED,
        MULTITAG
    }
}
