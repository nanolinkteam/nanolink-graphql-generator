using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    public class PUserMutation
    {
        public string MobilePhoneNumber { get; set; }
        public string MobileCountryCode { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<ObjectId> ObsoleteDeviceIds { get; set; }
        public string Lang { get; set; }
        public ObjectId? Id { get; set; }
        public ObjectId GroupId { get; set; }
        public int? Version { get; set; }
        public List<string> ExternalKeys { get; set; }
        public string ExternalBag { get; set; }
    }
}
