using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    public class PLocationMutation
    {
        public string Name { get; set; }
        public PNominatimContainer Address { get; set; }
        public PGPSLocationInfo FixedGPSPosition { get; set; }
        public ObjectId? ParentReferenceId { get; set; }
        public List<ObjectId> GateIds { get; set; }
        public ObjectId? Id { get; set; }
        public ObjectId GroupId { get; set; }
        public int? Version { get; set; }
        public List<string> ExternalKeys { get; set; }
        public string ExternalBag { get; set; }
    }
}
