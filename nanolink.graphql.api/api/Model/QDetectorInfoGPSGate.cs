using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QDetectorInfoGPSGate
    {
        public virtual int AllTagsFound { get => default; set {} }
        public virtual DateTime CreatedDate { get => default; set {} }
        public virtual ObjectId DetectorId { get => default; set {} }
        public virtual bool? GpsSupported { get => default; set {} }
        public virtual QGPSLocationInfo Location { get => default; set {} }
        public virtual string Model { get => default; set {} }
        public virtual int NanoLinksFound { get => default; set {} }
        public virtual string PID { get => default; set {} }
        public virtual ObjectId ReferenceId { get => default; set {} }
        public virtual int VersionCode { get => default; set {} }
        public virtual string VersionName { get => default; set {} }
        public virtual string VID { get => default; set {} }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
