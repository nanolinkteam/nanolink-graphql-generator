using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QGPSLocationInfo
    {
        public virtual double Accuracy { get => default; set {} }
        public virtual double Altitude { get => default; set {} }
        public virtual double Bearing { get => default; set {} }
        public virtual DateTime Date { get => default; set {} }
        public virtual double Latitude { get => default; set {} }
        public virtual double Longitude { get => default; set {} }
        public virtual double Speed { get => default; set {} }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
