using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    public class PServiceData
    {
        public string Type { get; set; }
        public PServiceDataPeriodic ServiceDataPeriodic { get; set; }
        public PServiceDataOneshot ServiceDataOneshot { get; set; }
        public PServiceDataWarranty ServiceDataWarranty { get; set; }
    }
}
