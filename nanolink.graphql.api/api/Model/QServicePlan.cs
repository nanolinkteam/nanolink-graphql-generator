using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    [Polymorph(typeof(QServicePlanPeriodic),typeof(QServicePlanOneshot),typeof(QServicePlanWarranty))]
    public abstract class QServicePlan
    {
        public virtual string CreatedBy { get => default; set {} }
        public virtual QReference CreatedByUser { get => default; set {} }
        public virtual DateTime CreatedDate { get => default; set {} }
        public virtual DateTime CreatedDateTime { get => default; set {} }
        public virtual string Description { get => default; set {} }
        public virtual int DueSlackInDays { get => default; set {} }
        public virtual string ExternalBag { get => default; set {} }
        public virtual List<string> ExternalKeys { get => default; set {} }
        public virtual string Id { get => default; set {} }
        public virtual string Name { get => default; set {} }
        public virtual int Version { get => default; set {} }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
