using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    public class PNominatimContainer
    {
        public string House_number { get; set; }
        public string Road { get; set; }
        public string Postcode { get; set; }
        public string Attraction { get; set; }
        public string Pedestrian { get; set; }
        public string Village { get; set; }
        public string Town { get; set; }
        public string City { get; set; }
        public string Suburb { get; set; }
        public string City_district { get; set; }
        public string State { get; set; }
        public string State_district { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string Country_code { get; set; }
        public string Region { get; set; }
        public string Island { get; set; }
        public string Hamlet { get; set; }
        public string Locality { get; set; }
        public string Farm { get; set; }
        public string Airport { get; set; }
        public string House_name { get; set; }
        public string House { get; set; }
        public string Moor { get; set; }
        public string Islet { get; set; }
        public string Houses { get; set; }
        public string Junction { get; set; }
    }
}
