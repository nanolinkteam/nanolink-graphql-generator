using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    [Polymorph(typeof(QServiceDataPeriodic),typeof(QServiceDataOneshot),typeof(QServiceDataWarranty))]
    public abstract class QServiceData
    {
        public virtual DateTime? Due { get => default; set {} }
        public virtual int DueSlackInDays { get => default; set {} }
        public virtual string Id { get => default; set {} }
        public virtual QServiceLog LastLog { get => default; set {} }
        public virtual DateTime? LastServiceDate { get => default; set {} }
        public virtual QServicePlanPeriodic ServicePlan { get => default; set {} }
        public virtual string ServicePlanId { get => default; set {} }
        public virtual bool ServiceWasPerformed { get => default; set {} }
        public virtual QReference User { get => default; set {} }
        public virtual string UserId { get => default; set {} }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
