using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    public class PServiceDataWarranty
    {
        public DateTime PurchaseDate { get; set; }
        public ObjectId? Id { get; set; }
        public ObjectId ServicePlanId { get; set; }
        public int DueSlackInDays { get; set; }
    }
}
