using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QNominatimContainer
    {
        public virtual string Airport { get => default; set {} }
        public virtual string Attraction { get => default; set {} }
        public virtual string City { get => default; set {} }
        public virtual string City_district { get => default; set {} }
        public virtual string Country { get => default; set {} }
        public virtual string Country_code { get => default; set {} }
        public virtual string County { get => default; set {} }
        public virtual string Farm { get => default; set {} }
        public virtual string Hamlet { get => default; set {} }
        public virtual string House { get => default; set {} }
        public virtual string House_name { get => default; set {} }
        public virtual string House_number { get => default; set {} }
        public virtual string Houses { get => default; set {} }
        public virtual string Island { get => default; set {} }
        public virtual string Islet { get => default; set {} }
        public virtual string Junction { get => default; set {} }
        public virtual string Locality { get => default; set {} }
        public virtual string Moor { get => default; set {} }
        public virtual string Pedestrian { get => default; set {} }
        public virtual string Postcode { get => default; set {} }
        public virtual string Region { get => default; set {} }
        public virtual string Road { get => default; set {} }
        public virtual string State { get => default; set {} }
        public virtual string State_district { get => default; set {} }
        public virtual string Suburb { get => default; set {} }
        public virtual string Town { get => default; set {} }
        public virtual string Village { get => default; set {} }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
