using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QServiceDataPeriodic: QServiceData
    {
        public virtual ServicePlanIntervalType IntervalType { get => default; set {} }
        public virtual ServicePlanPeriod Period { get => default; set {} }
        public virtual DateTime? StartDate { get => default; set {} }
    }
}
