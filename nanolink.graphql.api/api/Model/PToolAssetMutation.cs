using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    public class PToolAssetMutation
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public string KeyWords { get; set; }
        public string Description { get; set; }
        public string Serial { get; set; }
        public string VID { get; set; }
        public string TagType { get; set; }
        public int? Version { get; set; }
        public ObjectId? Id { get; set; }
        public List<ObjectId> CategoryIds { get; set; }
        public string Image { get; set; }
        public bool? UpdateImage { get; set; }
        public List<PDocumentMutation> DocumentList { get; set; }
        public List<PServiceData> ServiceData { get; set; }
        public List<string> ExternalKeys { get; set; }
        public string ExternalBag { get; set; }
    }
}
