using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QDetectorInfoMobile
    {
        public virtual AddOnState? AddOnState { get => default; set {} }
        public virtual int AllTagsFound { get => default; set {} }
        public virtual bool? BatteryOptimized { get => default; set {} }
        public virtual bool? BluetoothEnabled { get => default; set {} }
        public virtual bool? BluetoothFailure { get => default; set {} }
        public virtual bool? BluetoothSupported { get => default; set {} }
        public virtual DateTime CreatedDate { get => default; set {} }
        public virtual ObjectId DetectorId { get => default; set {} }
        public virtual bool? DeviceIdleMode { get => default; set {} }
        public virtual bool? GpsEnabled { get => default; set {} }
        public virtual bool? GpsSupported { get => default; set {} }
        public virtual string Ip { get => default; set {} }
        public virtual bool? IsDeviceAdmin { get => default; set {} }
        public virtual bool? KnoxLicenseActivated { get => default; set {} }
        public virtual QKnoxLicenseInfo KnoxLicenseInfo { get => default; set {} }
        public virtual QGPSLocationInfo LastKnownLocation { get => default; set {} }
        public virtual DateTimeOffset? LocalDeviceTime { get => default; set {} }
        public virtual QGPSLocationInfo Location { get => default; set {} }
        public virtual int NanoLinksFound { get => default; set {} }
        public virtual bool? NetworkAvailable { get => default; set {} }
        public virtual string NetworkType { get => default; set {} }
        public virtual OsType? OS { get => default; set {} }
        public virtual string PhoneBrand { get => default; set {} }
        public virtual string PhoneDevice { get => default; set {} }
        public virtual string PhoneManufacturer { get => default; set {} }
        public virtual string PhoneModel { get => default; set {} }
        public virtual string PhoneProduct { get => default; set {} }
        public virtual string PhoneReleaseVersion { get => default; set {} }
        public virtual bool? PowerSaveMode { get => default; set {} }
        public virtual ObjectId ReferenceId { get => default; set {} }
        public virtual DateTime? RegistrationDate { get => default; set {} }
        public virtual string RegistrationId { get => default; set {} }
        public virtual double? TimeOffset { get => default; set {} }
        public virtual int? VersionCode { get => default; set {} }
        public virtual string VersionName { get => default; set {} }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
