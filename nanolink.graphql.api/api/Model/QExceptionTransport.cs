using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QExceptionTransport
    {
        public virtual string DetailDescription { get => default; set {} }
        public virtual string ErrorKey { get => default; set {} }
        public virtual string Message { get => default; set {} }
        public virtual string StackTrace { get => default; set {} }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
