﻿using System;
using System.Collections.Generic;
using System.Text;

namespace nanolink.graphql.api
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ResolvePathAttribute: Attribute
    {
        public string Path { get; set; }

        public ResolvePathAttribute(string path)
        {
            Path = path;
        }
    }
}
