using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QDocument
    {
        public virtual DateTime CreatedDate { get => default; set {} }
        public virtual DateTime CreatedDateTime { get => default; set {} }
        public virtual string ExternalBag { get => default; set {} }
        public virtual List<string> ExternalKeys { get => default; set {} }
        public virtual string FileName { get => default; set {} }
        public virtual string Id { get => default; set {} }
        public virtual string MimeType { get => default; set {} }
        public virtual string Name { get => default; set {} }
        public virtual string Url { get => default; set {} }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
