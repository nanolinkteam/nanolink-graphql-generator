using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    [Polymorph(typeof(QUser),typeof(QLocation),typeof(QAsset))]
    public abstract class QReference
    {
        public virtual List<QCategory> Categories { get => default; set {} }
        public virtual DateTime CreatedDateTime { get => default; set {} }
        public virtual List<QMobileDevice> Devices { get => default; set {} }
        public virtual List<QDocument> Documents { get => default; set {} }
        public virtual string ExternalBag { get => default; set {} }
        public virtual List<string> ExternalKeys { get => default; set {} }
        public virtual string FoundBy { get => default; set {} }
        public virtual DateTime? FoundByTime { get => default; set {} }
        public virtual string GroupId { get => default; set {} }
        public virtual string GroupName { get => default; set {} }
        public virtual string Id { get => default; set {} }
        public virtual QBinaryBlob Image { get => default; set {} }
        public virtual QNanoLink NanoLink { get => default; set {} }
        public virtual List<QServiceData> ServiceData { get => default; set {} }
        public virtual DateTime? ServiceDue { get => default; set {} }
        public virtual int ServiceStatus { get => default; set {} }
        public virtual int Version { get => default; set {} }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
