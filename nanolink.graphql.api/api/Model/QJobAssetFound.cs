using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QJobAssetFound: QJobAsset
    {
        public virtual bool Disabled { get => default; set {} }
        public virtual List<QJobGroupInfo> Groups { get => default; set {} }
        public virtual ObjectId Id { get => default; set {} }
        public virtual List<QReference> References { get => default; set {} }
        public virtual int Version { get => default; set {} }
    }
}
