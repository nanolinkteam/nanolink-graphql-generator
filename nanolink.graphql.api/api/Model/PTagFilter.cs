using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    public class PTagFilter
    {
        public bool? WithNanoLink { get; set; }
        public bool? WithQRCode { get; set; }
        public bool? WithTracker { get; set; }
        public bool? WithGPSGate { get; set; }
    }
}
