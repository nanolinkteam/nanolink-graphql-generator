﻿using System;
using System.Collections.Concurrent;

namespace nanolink.graphql.api
{
    public static class QResultExtensions
    {
        private static ConcurrentDictionary<Type, string> _typeResolverCache = new ConcurrentDictionary<Type, string>();
        private static string GetGraphQLName(Type type)
        {
            return _typeResolverCache.GetOrAdd(type, (n) => type.GraphQLName());
        }
        public static Func<string, string, Type> ResolveTypeFunc(this Type t)
        {
            return (nspc, n) =>
            {
                string name = GetGraphQLName(t);
                string tname;
                if (n == name)
                    tname = $"{t.Namespace}.{t.Name}";
                else
                    tname = $"{t.Namespace}.{n.Substring(1)}";
                return t.Assembly.GetType(tname);
            };
        }
    }
}
