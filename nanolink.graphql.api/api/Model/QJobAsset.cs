using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    [Polymorph(typeof(QJobAssetFound))]
    public abstract class QJobAsset
    {
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
