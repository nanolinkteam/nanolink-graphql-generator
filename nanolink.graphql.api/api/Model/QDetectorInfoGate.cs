using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QDetectorInfoGate
    {
        public virtual int AllTagsFound { get => default; set {} }
        public virtual string BluetoothMac { get => default; set {} }
        public virtual DateTime CreatedDate { get => default; set {} }
        public virtual ObjectId DetectorId { get => default; set {} }
        public virtual string GateDevice { get => default; set {} }
        public virtual bool? GpsSupported { get => default; set {} }
        public virtual QGPSLocationInfo Location { get => default; set {} }
        public virtual int NanoLinksFound { get => default; set {} }
        public virtual bool NetworkAvailable { get => default; set {} }
        public virtual string NetworkType { get => default; set {} }
        public virtual ObjectId ReferenceId { get => default; set {} }
        public virtual int RequestInterval { get => default; set {} }
        public virtual bool Throttle { get => default; set {} }
        public virtual int VersionCode { get => default; set {} }
        public virtual string VersionName { get => default; set {} }
        public virtual string WifiMac { get => default; set {} }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
