using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    public enum ServicePlanPeriod
    {
        EVERY_10_YEARS,
        EVERY_9_YEARS,
        EVERY_8_YEARS,
        EVERY_7_YEARS,
        EVERY_6_YEARS,
        EVERY_5_YEARS,
        EVERY_4_YEARS,
        EVERY_3_YEARS,
        EVERY_2_YEARS,
        EVERY_23_MONTH,
        EVERY_22_MONTH,
        EVERY_21_MONTH,
        EVERY_20_MONTH,
        EVERY_19_MONTH,
        EVERY_18_MONTH,
        EVERY_17_MONTH,
        EVERY_16_MONTH,
        EVERY_15_MONTH,
        EVERY_14_MONTH,
        EVERY_13_MONTH,
        EVERY_12_MONTH,
        EVERY_11_MONTH,
        EVERY_10_MONTH,
        EVERY_9_MONTH,
        EVERY_8_MONTH,
        EVERY_7_MONTH,
        EVERY_6_MONTH,
        EVERY_5_MONTH,
        EVERY_4_MONTH,
        EVERY_3_MONTH,
        EVERY_2_MONTH,
        EVERY_MONTH,
        EVERY_2_WEEK,
        EVERY_WEEK,
        EVERY_2_DAY,
        DAILY
    }
}
