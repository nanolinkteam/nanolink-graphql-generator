using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    public class PSortKey
    {
        public string Field { get; set; }
        public bool? IsDescending { get; set; }
    }
}
