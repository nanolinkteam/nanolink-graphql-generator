using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QLocation: QReference
    {
        public virtual QNominatimContainer Address { get => default; set {} }
        public virtual DateTime CreatedDate { get => default; set {} }
        public virtual QGPSLocationInfo FixedGPSPosition { get => default; set {} }
        public virtual string Name { get => default; set {} }
        public virtual string ParentReferenceId { get => default; set {} }
    }
}
