using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    [Polymorph(typeof(QGateDevice),typeof(QMobileDevice),typeof(QTrackerDevice),typeof(QGPSGateDevice))]
    public abstract class QDeviceBase
    {
        public virtual string Code { get => default; set {} }
        public virtual DateTime CreatedDateTime { get => default; set {} }
        public virtual string DeviceName { get => default; set {} }
        public virtual string ExternalBag { get => default; set {} }
        public virtual List<string> ExternalKeys { get => default; set {} }
        public virtual string Id { get => default; set {} }
        public virtual QReference Reference { get => default; set {} }
        public virtual ObjectId ReferenceId { get => default; set {} }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
