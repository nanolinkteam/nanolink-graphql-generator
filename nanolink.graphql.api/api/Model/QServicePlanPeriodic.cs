using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QServicePlanPeriodic: QServicePlan
    {
        public virtual ServicePlanIntervalType IntervalType { get => default; set {} }
        public virtual ServicePlanPeriod Period { get => default; set {} }
    }
}
