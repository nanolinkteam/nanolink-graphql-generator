using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QCategory
    {
        public virtual int AssetCount { get => default; set {} }
        public virtual string Id { get => default; set {} }
        public virtual string Image { get => default; set {} }
        public virtual bool LastSibling { get => default; set {} }
        public virtual int Level { get => default; set {} }
        public virtual string Name { get => default; set {} }
        public virtual string ParentId { get => default; set {} }
        public virtual int Version { get => default; set {} }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
