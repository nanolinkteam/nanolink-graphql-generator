using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QTrackerDevice: QDeviceBase
    {
        public virtual QDetectorInfoTracker LastTrackerInfo { get => default; set {} }
    }
}
