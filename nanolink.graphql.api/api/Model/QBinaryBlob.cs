using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QBinaryBlob
    {
        public virtual string ContentType { get => default; set {} }
        public virtual string DataBase64
        {
            get { return Convert.ToBase64String(Data); }
            set
            {
                Data = value != null ? Data = Convert.FromBase64String(value) : null;
            }
        }
        [JsonIgnore]
        [System.Text.Json.Serialization.JsonIgnore]
        public byte[] Data { get; set; }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
