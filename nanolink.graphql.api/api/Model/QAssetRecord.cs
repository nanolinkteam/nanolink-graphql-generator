using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QAssetRecord
    {
        public virtual double A { get => default; set {} }
        public virtual DateTime CreatedDateTime { get => default; set {} }
        public virtual DateTime D { get => default; set {} }
        public virtual QDeviceBase Device { get => default; set {} }
        public virtual string ExternalBag { get => default; set {} }
        public virtual List<string> ExternalKeys { get => default; set {} }
        public virtual int? F { get => default; set {} }
        public virtual string I { get => default; set {} }
        public virtual string Id { get => default; set {} }
        public virtual QGeoPosition L { get => default; set {} }
        public virtual string M { get => default; set {} }
        public virtual string P { get => default; set {} }
        public virtual int R { get => default; set {} }
        public virtual string V { get => default; set {} }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
