using System;
using System.Collections.Generic;
using MongoDB.Bson;
namespace nanolink.graphql.api
{
    public enum AddOnState
    {
        NOT_INSTALLED,
        MISSING_PERMISSIONS,
        INSTALLED
    }
}
