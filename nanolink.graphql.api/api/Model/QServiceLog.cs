using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
namespace nanolink.graphql.api
{
    public abstract class QServiceLog
    {
        public virtual string Comment { get => default; set {} }
        public virtual DateTime CreatedDateTime { get => default; set {} }
        public virtual ServiceLogEventCode EventCode { get => default; set {} }
        public virtual string ExternalBag { get => default; set {} }
        public virtual List<string> ExternalKeys { get => default; set {} }
        public virtual string Id { get => default; set {} }
        public virtual string ServiceDataId { get => default; set {} }
        public virtual QServicePlan ServicePlan { get => default; set {} }
        public virtual string ServicePlanId { get => default; set {} }
        public virtual DateTime Stamp { get => default; set {} }
        public virtual QUser User { get => default; set {} }
        public virtual string UserId { get => default; set {} }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
}
