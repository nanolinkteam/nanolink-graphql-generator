﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoDB.Bson.Serialization.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class BsonIdAttribute: Attribute
    {
    }
    [AttributeUsage(AttributeTargets.Property)]
    public class BsonIgnore: Attribute
    {
    }
}
