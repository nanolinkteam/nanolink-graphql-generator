﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace nanolink.graphql.api
{
    [JsonObject(NamingStrategyType = typeof(DefaultNamingStrategy))]
    public class ApiResult
    {
        public int GroupVersion { get; set; }
        public ExceptionTransport[] Errors { get; set; }
        public virtual void SetResult(object result)
        {
        }
        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });
    }
    public class ApiResult<T>: ApiResult
    {
        public T Result { get; set; }
        public override void SetResult(object result)
        {
            Result = (T)result;
        }
    }
}
