﻿using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace nanolink.graphql.api
{
    public static class GraphQLExtensions
    {
        public static Type GetEnumerable(this Type t)
        {
            if (t.Name == typeof(IEnumerable<>).Name)
                return t;
            else
                return t.GetInterface(typeof(IEnumerable<>).Name);
        }

        public static IEnumerable<PropertyInfo> OutputFields(this Type t)
        {
            foreach (var prop in t.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                bool include = prop.DeclaringType == t;
                foreach (var c in prop.GetCustomAttributes())
                {
                    if (c.GetType() == typeof(JsonIgnoreAttribute))
                    {
                        include = false;
                        break;
                    }
                }
                if (include)
                    yield return prop;
            }
        }

        private static Type ResolveType(Type topLevelType, Type type)
        {
            return topLevelType.Assembly.GetType($"{topLevelType.Namespace}.{type.Name.Substring(1)}") ?? type;
        }

        private static Type UnderlyingType(Type t)
        {
            Type tType = t;
            Type en;
            if ((en = t.GetEnumerable()) != null && en.IsConstructedGenericType)
            {
                tType = en.GenericTypeArguments[0];
            }
            return tType;
        }
        private static void UnfoldType(StringBuilder b, Type topLevelType, Type t)
        {
            Type tType = UnderlyingType(t);
            List<Type> ptypes = tType.GetPolymorphTypes().ToList();
            if (ptypes.Count > 0)
            {
                foreach (var n in ptypes)
                {
                    b.Append("__typename ");
                    b.Append("... on ");
                    b.Append(n.GraphQLName());
                    b.Append("{");
                    QueryFields(topLevelType, ResolveType(topLevelType, n), b);
                    b.Append("}");
                }
            }
            else
            {
                foreach (var n in tType.OutputFields())
                {
                    QueryField(topLevelType, n, b, ResolveType(topLevelType, n.PropertyType));
                }
            }
        }

        private static void QueryField(Type topLevelType, PropertyInfo p, StringBuilder b, Type targetType = null)
        {
            bool include = p.GetCustomAttribute<JsonIgnoreAttribute>() == null;
            if (include)
            {
                b.Append(p.Name.ToLowerCamelCase());
                b.Append(" ");
                targetType = targetType ?? p.PropertyType;
                var ut = UnderlyingType(targetType);
                if (!ut.IsValueType && ut != typeof(string))
                {
                    b.Append(" {");
                    UnfoldType(b, topLevelType, targetType);
                    b.Append("}");
                }
            }
        }
        private static void QueryFields(Type topLevelType, Type t, StringBuilder b)
        {
            UnfoldType(b, topLevelType, t);
        }
        private static ConcurrentDictionary<Type, string> _schemaCache = new ConcurrentDictionary<Type, string>();
        public static string QueryFields(this Type t)
        {
            return _schemaCache.GetOrAdd(t, (tt) =>
            {
                try
                {
                    StringBuilder b = new StringBuilder();
                    QueryFields(tt, tt, b);
                    return b.ToString();
                }
                catch (Exception e)
                {
                    throw e;
                }
            });
        }
        public static string ListToJson<T>(this List<T> l)
        {
            return JsonConvert.SerializeObject(l, new JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver(), NullValueHandling = NullValueHandling.Ignore });
        }
        public static string ListToJson<T>(this IEnumerable<T> l)
        {
            return JsonConvert.SerializeObject(l, new JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver(), NullValueHandling = NullValueHandling.Ignore });
        }
    }

}
