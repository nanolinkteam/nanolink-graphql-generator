﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;

namespace nanolink.graphql.api
{
    public class GraphQLContractResolver: DefaultContractResolver
    {
        private Type _topLevelType;
        public Assembly ModelAssembly { get; private set; }
        public string NameSpace { get; private set; }
        protected override JsonObjectContract CreateObjectContract(Type objectType)
        {
            Type t = objectType;
            if (!typeof(ApiResult).IsAssignableFrom(objectType))
            {
                var tname = $"{NameSpace}.{(objectType.Name.StartsWith("Q") ? objectType.Name.Substring(1) : objectType.Name)}";
                t = ModelAssembly.GetType(tname) ?? objectType;
            }
            return base.CreateObjectContract(t);
        }
        public GraphQLContractResolver(Type topLevelType)
        {
            _topLevelType = topLevelType;
            ModelAssembly = topLevelType.Assembly;
            NameSpace = topLevelType.Namespace;
        }
    }
    public class GraphQLClient: IDisposable
    {
        private string _graphBaseUrl = null;
        private HttpClient _client;
        public GraphQLClient(string baseUrl)
        {
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Add("Connection", "close");
            _graphBaseUrl = baseUrl;
        }

        private JsonSerializerSettings GetSerializerSettings(Type topLevelType)
        {
            return new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
                NullValueHandling = NullValueHandling.Ignore,
                Converters = { new ObjectIdConverter(), new GraphQLEnumJsonConverter() },
                ContractResolver = new GraphQLContractResolver(topLevelType),
                SerializationBinder = new CustomBinder(topLevelType.Assembly, topLevelType.Namespace)
            };
        }
        private JsonSerializer GetSerializer(Type topLevelType)
        {
            return JsonSerializer.Create(GetSerializerSettings(topLevelType));
        }

        private async Task<GraphQLResponse> PostQueryAsyncInternal(string url, string query, int? timeoutInSecs = null)
        {
            var q = JsonConvert.SerializeObject(new { Query = query });
            var response = await _client.PostAsync(url, new StringContent(q));
            response.EnsureSuccessStatusCode();
            GraphQLResponse retVal;
            using (var r = new StreamReader(await response.Content.ReadAsStreamAsync()))
            {
                var jreader = new JsonTextReader(r);
                var ser = JsonSerializer.Create();
                retVal = ser.Deserialize<GraphQLResponse>(jreader);
            }
            return retVal;
        }
        public static string BuildQuery<T>(string path, bool mutation, string operation, bool asList, string args = null)
        {
            string q;
            string r = null;
            path = path ?? "api/mobile";
            if (!typeof(T).IsValueType && typeof(T) != typeof(string))
            {
                if (asList)
                    r = $"{{ result {{{typeof(T).QueryFields()}}} errors {{{typeof(ExceptionTransport).QueryFields() }}} total }}";
                else
                    r = $"{{ result {{{typeof(T).QueryFields()}}} errors {{{typeof(ExceptionTransport).QueryFields() }}} }}";
            }
            else
            {
                if (asList)
                    r = $"{{ result errors {{{typeof(ExceptionTransport).QueryFields() }}} total }}";
                else
                    r = $"{{ result errors {{{typeof(ExceptionTransport).QueryFields() }}} }}";
            }
            if (args != null)
                q = $"{(mutation ? "mutation " : "")}{{ {operation}({args}) {r} }}";
            else
                q = $"{(mutation ? "mutation " : "")}{{ {operation} {r} }}";
            return q;
        }

        public async Task<ApiResult> Post<T, R>(string path, bool mutation, string operation, bool asList, Type topLevelType, string args = null, int? timeoutInSecs = 0) where R : ApiResult
        {
            ApiResult retVal = null;
            var res = await PostQueryAsyncInternal($"{_graphBaseUrl}/{path}",
                    BuildQuery<T>(path, mutation, operation, asList, args), timeoutInSecs);
            if (res == null)
            {
                retVal = Activator.CreateInstance<R>();
                retVal.Errors = new ExceptionTransport[] { new Exception("Null result returned from server.") };
            }
            else
            {
                var retObject = (JObject)(res.Data[operation]);
                if (retObject == null)
                {
                    retObject = (JObject)(res.Data["graphql"]);
                }
                try
                {
                    return JsonConvert.DeserializeObject<R>(retObject.FromGraphQLJson(), GetSerializerSettings(topLevelType));
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return retVal;
        }
        public async Task<ApiResult> Post<T>(string path, bool mutation, string operation, bool asList, string args = null, int? timeoutInSecs = null)
        {
            if (asList)
                return await Post<T, ApiResultList<T>>(path, mutation, operation, asList, typeof(T), args, timeoutInSecs);
            else
                return await Post<T, ApiResult<T>>(path, mutation, operation, asList, typeof(T), args, timeoutInSecs);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _client.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~GraphQLClient()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
