﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Text.Json;

namespace nanolink.graphql.api
{
    public class GraphQLNewtonsoftContractResolver: Newtonsoft.Json.Serialization.DefaultContractResolver
    {
        protected override Newtonsoft.Json.Serialization.JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var retVal = base.CreateProperty(member, memberSerialization);
            retVal.ShouldSerialize = (t) => !retVal.DeclaringType.IsAbstract;
            return retVal;
        }
    }
}
