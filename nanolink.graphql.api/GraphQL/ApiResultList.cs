﻿using System.Collections.Generic;

namespace nanolink.graphql.api
{
    public class ApiResultList<T> : ApiResult
    {
        public int Total { get; set; }
#if !PCL
        public IEnumerable<T> Result { get; set; }

        public override void SetResult(object result)
        {
            Result = (IEnumerable<T>)result;
        }
#else
        public List<T> Result { get; set; }
#endif
    }

    public class ApiResultListWithVersion<T>: ApiResultList<T>
    {
        public int Version { get; set; }
    }
}
