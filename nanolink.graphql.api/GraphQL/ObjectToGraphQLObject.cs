﻿using Newtonsoft.Json;
using System;
using System.Reflection;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;

namespace nanolink.graphql.api
{
    public class CustomBinder : ISerializationBinder
    {
        public Assembly ModelAssembly { get; private set; }
        public string NameSpace { get; private set; } 
        
        public CustomBinder(Assembly assembly, String nameSpace)
        {
            ModelAssembly = assembly;
            NameSpace = nameSpace;
        }
        public CustomBinder(Type topLevelType)
        {
            ModelAssembly = topLevelType.Assembly;
            NameSpace = topLevelType.Namespace;
        }
        public void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            assemblyName = serializedType.Assembly.GetName().Name;
            typeName = $"{serializedType.Namespace}.{serializedType.Name}";
        }
        public Type BindToType(string assemblyName, string typeName)
        {
            var tname = $"{NameSpace}.{(typeName.StartsWith("Q") ? typeName.Substring(1) : typeName)}";
            var type = ModelAssembly.GetType(tname);
            return type;
        }
    }
    public static class ObjectToGraphQLObject
    {
        public class GraphQLJsonWriter : JsonTextWriter
        {
            private bool _upperCamelCase = false;
            public int? ApplyLevel { get; set; }
            public Func<string, string> TypeConverter { get; private set; }

            public GraphQLJsonWriter(TextWriter textWriter, bool upperCamelCase = false, int? applyLevel = null) : base(textWriter)
            {
                QuoteName = false;
                _upperCamelCase = upperCamelCase;
                ApplyLevel = applyLevel;
            }
            private string Convert(string name)
            {
                string retVal;
                if (_upperCamelCase && name == "__typename")
                {
                    retVal = "$type";
                }
                else
                if (!_upperCamelCase && name == "$type")
                {
                    retVal = "__typename";
                }
                else
                {
                    if (_upperCamelCase)
                        retVal = name.ToUpperCamelCase();
                    else
                        retVal = name.ToLowerCamelCase();
                }
                return retVal;
            }
            public override void WritePropertyName(string name)
            {
                if (!ApplyLevel.HasValue || Top > ApplyLevel.Value)
                {
                    name = Convert(name);
                }
                base.WritePropertyName(name);
            }
            public override void WritePropertyName(string name, bool escape)
            {
                if (!ApplyLevel.HasValue || Top > ApplyLevel.Value)
                {
                    name = Convert(name);
                }
                base.WritePropertyName(name, escape);
            }
            public override Task WritePropertyNameAsync(string name, bool escape, CancellationToken cancellationToken = default(CancellationToken))
            {
                if (!ApplyLevel.HasValue || Top > ApplyLevel.Value)
                {
                    name = Convert(name);
                }
                return base.WritePropertyNameAsync(name, escape, cancellationToken);
            }
            public override Task WritePropertyNameAsync(string name, CancellationToken cancellationToken = default(CancellationToken))
            {
                if (!ApplyLevel.HasValue || Top > ApplyLevel.Value)
                {
                    name = Convert(name);
                }
                return base.WritePropertyNameAsync(name, cancellationToken);
            }
        }
        public static string ToGraphQLJson<T>(this T obj)
        {
            var ser = JsonSerializer.Create(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, Converters = { new ObjectIdConverter(), new NullableObjectIdConverter() } });
            StringWriter w = new StringWriter();
            using (JsonTextWriter jsonWriter = new GraphQLJsonWriter(w))
            {
                ser.Serialize(jsonWriter, obj, typeof(T));
            }
            return w.ToString();
        }
        public static string FromGraphQLJson<T>(this T obj, int? applyLevel = null)
        {
            var ser = JsonSerializer.Create();
            StringWriter w = new StringWriter();

            using (JsonTextWriter jsonWriter = new GraphQLJsonWriter(w, true, applyLevel))
            {
                ser.Serialize(jsonWriter, obj, typeof(T));
            }
            return w.ToString();
        }
        public static void FromGraphQLJson(this object obj, Stream output, int? applyLevel = null)
        {
            var ser = JsonSerializer.Create();
            using (var writer = new StreamWriter(output))
            {
                using (var jwriter = new GraphQLJsonWriter(writer, true, applyLevel) { QuoteName = true })
                {
                    ser.Serialize(jwriter, obj);
                }
            }
        }
    }
}
