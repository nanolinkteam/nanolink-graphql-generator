using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Concurrent;

namespace nanolink.graphql.api
{
    public class GraphQLEnumJsonConverter : JsonConverter
    {
        private static ConcurrentDictionary<string, string> _nameMap = new ConcurrentDictionary<string, string>();
        public static string GraphQLEnumName(string v)
        {
            return _nameMap.GetOrAdd(v, (n) =>
            {
                return string.Join("_", (from w in n.ToWords() select w.ToUpperInvariant()));
            });
        }
        public static T ConvertEnum<T>(string enumText)
        {
            return (T) ConvertEnum(typeof(T), enumText);
        }
        public static object ConvertEnum(Type enumType, string enumText)
        {
            var name = (from n in Enum.GetNames(enumType)
                        where GraphQLEnumName(n) == enumText || n == enumText
                        select n).FirstOrDefault();
            return Enum.Parse(enumType, name);
        }

        public override bool CanConvert(Type objectType)
        {
            bool isNullable = objectType.IsConstructedGenericType && objectType.GetGenericTypeDefinition() == typeof(Nullable<>);
            return isNullable ? Nullable.GetUnderlyingType(objectType).IsEnum :  objectType.IsEnum;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            bool isNullable = objectType.IsConstructedGenericType && objectType.GetGenericTypeDefinition() == typeof(Nullable<>);
            if (reader.TokenType == JsonToken.Null)
            {
                if (!isNullable)
                {
                    throw new Exception("Cannot assign null to non-nullable enum");
                }
                return null;
            }
            Type t = isNullable ? Nullable.GetUnderlyingType(objectType) : objectType;

            try
            {
                if (reader.TokenType == JsonToken.String)
                {
                    string enumText = reader.Value.ToString();
                    return ConvertEnum(t, enumText);
                }
            }
            catch
            {
                throw new Exception("Error trying to parse enum value");
            }
            throw new Exception("Unexception token trying to parse enum");
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }
            Enum e = (Enum)value;
            writer.WriteValue(GraphQLEnumName(e.ToString()));
        }
    }
}
