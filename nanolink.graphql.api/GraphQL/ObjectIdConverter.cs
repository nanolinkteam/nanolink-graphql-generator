﻿using MongoDB.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace nanolink.graphql.api
{
    public class ObjectIdConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value.ToString());
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken token = JToken.Load(reader);
            try
            {
                return new ObjectId(token.ToObject<string>());
            }
            catch (Exception e)
            {
                throw new Exception("Cannot convert source ObjectId to ObjectId. ", e);
            }
        }

        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(ObjectId));
        }
    }
    public class NullableObjectIdConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value?.ToString());
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken token = JToken.Load(reader);
            var obj = token.ToObject<string>();
            if (obj != null)
                return new ObjectId(token.ToObject<string>());
            else
                return new ObjectId?();
        }

        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(ObjectId?));
        }
    }
}
