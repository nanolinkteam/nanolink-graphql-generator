﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace nanolink.graphql.api
{
    public class GraphQLResponse
    {
        public dynamic Data { get; set; }
        public GraphQLError[] Errors { get; set; }
    }

    public class GraphQLError
    {
        public string Message { get; set; }
        public GraphQLLocation[] Locations { get; set; }
        [JsonExtensionData]
        public IDictionary<string, JToken> AdditonalEntries { get; set; }
    }
    public class GraphQLLocation 
    {
        public uint Column { get; set; }
        public uint Line { get; set; }
    }

}
