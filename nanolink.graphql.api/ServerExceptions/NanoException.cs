﻿using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;

namespace nanolink.graphql.api
{
    [JsonConverter(typeof(NanoExceptionJsonConverter))]
    public class NanoException : Exception
    {
#if !PCL
        public NanoException(SerializationInfo info, StreamingContext context)
        {
            if (info != null)
            {
                ErrorKey = info.GetString("ErrorKey");
                _Message = info.GetString("Message");
            }
        }
#endif

        public override string Message
        {
            get {
                if (_Message != "") {
                    return _Message;
                } else{
                    return base.Message;
                };
            }
        }

        private string _Message = "";

        public NanoException(string message, string errorKey = "errors.generic")
            : base(message)
        {

            ErrorKey = errorKey;
        }
        public string ErrorKey { get; set; }

    }
}
