﻿using System;
using System.Collections.Generic;
using System.Text;

namespace nanolink.graphql.api
{
    public class FieldExceptionDetail
    {
        public string CollectionName { get; set; }
        public string FieldName { get; set; }
        public string CollectionNameKey { get; set; }
        public string FieldNameKey { get; set; }
    }
}
