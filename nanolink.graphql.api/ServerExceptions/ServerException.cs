﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace nanolink.graphql.api
{
    public class ServerException: NanoException
    {
#if !PCL
        public ServerException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
#endif
        public ServerException(string message, string errorKey) : base(message, errorKey)
        {
        }

        public virtual string DetailDescription
        {
            get { return "{}"; }
            protected set { }
        }
    }
    public class ServerException<T>: ServerException
    {
#if !PCL
        public ServerException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            if (info != null)
            {
                DetailDescription = info.GetString("DetailDescription");
            }
        }
#endif
        public ServerException(string message, T details, string errorKey) : base(message, errorKey)
        {
            Details = details;
        }
        public T Details { get; private set; }
        public override string DetailDescription
        {
            get
            {
                if (Details != null)
                {
                    return JsonConvert.SerializeObject(Details);
                }
                else
                {
                    return null;
                }
            }
            protected set
            {
                try
                {
                    if (value != null)
                    {
                        Details = JsonConvert.DeserializeObject<T>(value);
                    }
                } catch { }
            }
        }
    }
}
