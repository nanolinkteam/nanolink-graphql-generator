﻿using System;

namespace nanolink.graphql.api
{
    public class ExceptionTransport
    {
        protected Exception _exception;
        private string _message;
        private string _stacktrace;

        public string StackTrace
        {
            get => _exception?.StackTrace ?? _stacktrace;
            set
            {
                if (_exception == null)
                    _stacktrace = value;
            }
        }            
        public string Message
        {
            get { return _exception?.Message ?? _message; }
            set
            {
                if (_exception == null)
                    _message = value;
            }
        }
        public string ErrorKey { get; set; }
        public string DetailDescription { get;set; }

        public ExceptionTransport()
        {
        }


        public ExceptionTransport(Exception exp, string errorKey = "errors.generic", string detailDescription = null)
        {
            var serverexp = exp as ServerException;
            if (serverexp != null)
            {
                ErrorKey = serverexp.ErrorKey;
                DetailDescription = serverexp.DetailDescription;
            }
            else
            {
                if (exp.InnerException != null)
                    exp = exp.InnerException;
                ErrorKey = errorKey;
                DetailDescription = detailDescription;
            }
            _exception = exp;
        }

        public Exception GetException() { return _exception; }

        public static implicit operator ExceptionTransport(Exception exp)
        {
            return new ExceptionTransport(exp);
        }
        public static implicit operator Exception(ExceptionTransport t)
        {
            return t.GetException();
        }
    }
}
