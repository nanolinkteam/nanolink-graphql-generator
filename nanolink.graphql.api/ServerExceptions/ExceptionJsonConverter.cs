﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace nanolink.graphql.api
{
    public class NanoExceptionJsonConverter : JsonConverter
    {
        public override Boolean CanConvert(Type objectType)
        {
#if PCL
            return typeof(NanoException).GetTypeInfo().IsAssignableFrom(objectType.GetTypeInfo());
#else
            return typeof(Exception).IsAssignableFrom(objectType);
#endif
        }

        public override Object ReadJson(JsonReader reader, Type objectType, Object existingValue, JsonSerializer serializer)
        {
            return new NanoException((existingValue as NanoException).Message, (existingValue as NanoException).ErrorKey);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }
            var exception = (Exception)value;
            
            
            writer.WriteStartObject();

            Type type = value.GetType();
            var typeName = string.Format("{0}, {1}", type.FullName, type.Namespace);

            writer.WritePropertyName("$type");
            writer.WriteValue(typeName);

            writer.WritePropertyName("Message");
            writer.WriteValue(exception.Message);
            writer.WritePropertyName("ErrorKey");
            writer.WriteValue((exception as NanoException).ErrorKey);

            writer.WriteEndObject();
        }
    }
}
