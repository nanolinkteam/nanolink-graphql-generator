﻿using System;
using System.Linq;
using MongoDB.Bson;
using nanolink.graphql.api;

namespace nanolink.graphql.api.example
{
    class Program
    {
        static string apitoken = ""; // Insert your apitoken here
        static string server = "https://dk1.nanolink.com";

        static async void Test()
        {
            var con = new GraphQLClient(server);
            try
            {
                Console.WriteLine("Try login");
                var login = await con.Auth_Externallogin(apitoken);
                if (login.Result != null)
                {
                    Console.WriteLine("Did login :-)");
                    var r = await con.Assets_Search<model.AssetModel>(login.Result,100,1);
                    if (r.Errors?.Length > 0)
                        Console.WriteLine(r.Errors[0].Message);
                    else
                    {
                        foreach (var n in r.Result)
                        {
                            Console.WriteLine(n.ToJson());
                        }
                    }
                }
                var cat = await con.Categories_Getall<model.Category>(login.Result);
                var asset = new PToolAssetMutation
                {
                    Brand = "Hello",
                    Model = "World",
                    // Do not ever use the root category. NEVER.
                    CategoryIds = new System.Collections.Generic.List<ObjectId> { ObjectId.Parse(cat.Result.Skip(1).First().Id) }
                };
                var retasset = (await con.Assets_Savetoolasset<model.AssetModel>(login.Result, asset)).Result;
                asset.Id = retasset.Id;
                asset.Version = retasset.Version;
                asset.Brand = "Hi";
                retasset = (await con.Assets_Savetoolasset<model.AssetModel>(login.Result, asset)).Result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        static async void Test2()
        {
            var con = new GraphQLClient(server);
            try
            {
                Console.WriteLine("Try login");
                var login = await con.Auth_Externallogin(apitoken);
                if (login.Result != null)
                {
                    Console.WriteLine("Logged in"); 
                    var res = await con.Assets_Logassets<Device.AssetRecord>(login.Result, DateTime.Parse("2020-10-14 07:00"));
                    foreach (var n in res.Result)
                    {
                        Console.WriteLine(n.Device?.GetType()?.Name);
                        Console.WriteLine(n.Device?.Id);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        static async void Test4()
        {
            var con = new GraphQLClient(server);
            try
            {
                Console.WriteLine("Try login");
                var login = await con.Auth_Externallogin(apitoken);
                if (login.Result != null)
                {
                    Console.WriteLine("Logged in");
                    var connector1 = await con.Links_List_Foundby_Asset<minmodel.Reference>(login.Result, "5eb957f854cadf0cc6e1926d", 360000);
                    var connector2 = await con.Links_List_Foundby_Asset<minmodel.Reference>(login.Result, "5eb9583354cadf0cc6e1d135", 8640000);
                    var connector3 = await con.Links_List_Foundby_Asset<minmodel.Reference>(login.Result, "5eb951bb54cadf0cc6dbd8f9", 8640000);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }


        static void Main(string[] args)
        {
            Test4();
            Console.ReadLine();
        }
    }
}
