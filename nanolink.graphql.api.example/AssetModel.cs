﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace nanolink.graphql.model
{

    // Keep the query result types in the same namespace, so the model can be populated correctly
    // Simply override the properties you wish to resolve
    public class AssetModel: api.QAsset // The toplevel model can have any name
    {
        public override string Id { get; set; }
        public override string Brand { get; set; }
        public override string Model { get; set; }
        public override api.QNanoLink NanoLink { get; set; }
        public override int Version { get; set; }
    }
    public class NanoLink : api.QNanoLink // Always name the class as baseclass not including the Q
    {
        public override string VID { get; set; }
        public override string Mac { get; set; }
        public override api.QAssetRecord LastKnownAssetRecords { get; set; }
    }
    public class AssetRecord : api.QAssetRecord
    {
        public override DateTime D { get; set; }
        public override string M { get; set; }
        public override string P { get; set; }
        public override api.QGeoPosition L { get; set; }
    }
    public class GeoPosition : api.QGeoPosition
    {
        public override double Longitude { get; set; }
        public override double Latitude { get; set; }
    }


    public class Category: api.QCategory
    {
        public override string Name { get; set; }
        public override string Id { get; set; }
    }
}
