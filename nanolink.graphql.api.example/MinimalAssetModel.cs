﻿using nanolink.graphql.api;
using System;
using System.Collections.Generic;
using System.Text;

namespace nanolink.graphql.minmodel
{
    [Polymorph(typeof(Asset), typeof(Location), typeof(User))]
    public class Reference: api.QReference
    {
    }
    public class Asset: Reference
    {   
        public override string Id { get; set; }
    }

    public class Location: Reference
    {
        public override string Id { get; set; }
    }
    public class User: Reference
    {
        public override string Id { get; set; }
    }
}
