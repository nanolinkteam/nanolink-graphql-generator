﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace nanolink.graphql.api.example.Device
{
    public class AssetRecord : api.QAssetRecord
    {
        public override DateTime D { get; set; }
        public override string M { get; set; }
        public override string P { get; set; }
        public override api.QGeoPosition L { get; set; }
        public override api.QDeviceBase Device { get; set; }
    }
    public class GeoPosition : api.QGeoPosition
    {
        public override double Longitude { get; set; }
        public override double Latitude { get; set; }
    }
    public class GateDevice: api.QGateDevice
    {
        public override string Id { get; set; }
        public override api.QDetectorInfoGate LastGateInfo { get; set; }
    }
    public class MobileDevice: api.QMobileDevice
    {
        public override string Id { get; set; }
        public override QDetectorInfoMobile LastMobileInfo { get; set; }
    }
    public class TrackerDevice: api.QTrackerDevice
    {
        public override string Id { get; set; }
        public override QDetectorInfoTracker LastTrackerInfo { get; set; }
    }
    public class GPSGateDevice: api.QGPSGateDevice
    {
        public override string Id { get; set; }
        public override QDetectorInfoGPSGate LastGPSGateInfo { get; set; }
    }
    public class DetectorInfoGate: api.QDetectorInfoGate
    {
        public override ObjectId ReferenceId { get; set; }
        public override string GateDevice { get; set; }
    }
    public class DetectorInfoMobile: api.QDetectorInfoMobile
    {
        public override ObjectId ReferenceId { get; set; }
        public override string PhoneModel { get; set; }
    }
    public class DetectorInfoTracker: api.QDetectorInfoTracker
    {
        public override ObjectId ReferenceId { get; set; }
    }
    public class DetectorInfoGPSGate: api.QDetectorInfoGPSGate
    {
        public override ObjectId ReferenceId { get; set; }
        public override string VID { get; set; }
    }
    public class User: api.QUser
    {
        public override string Id { get; set; }
        public override string FullName { get; set; }
    }
    public class Location: api.QLocation
    {
        public override string Id { get; set; }
        public override string Name { get; set; }
    }
    public class Asset: api.QAsset
    {
        public override string Id { get; set; }
        public override string Brand { get; set; }
        public override string Model { get; set; }
    }
}
