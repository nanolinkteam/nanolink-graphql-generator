﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nanolink.shared.Tools
{
    public static class Utility
    {
        public class Node<T>
        {
            public T Data;
            public T Parent;
            public int Level;
        }
        public static IEnumerable<T> Traverse<T>(this T item, Func<T, IEnumerable<T>> children)
        {
            var stack = new Stack<T>();
            stack.Push(item);
            while (stack.Any())
            {
                var next = stack.Pop();
                yield return next;
                var ch = children(next);
                if (ch != null)
                {
                    foreach (var child in ch)
                    {
                        stack.Push(child);
                    }
                }
            }
        }
        private static IEnumerable<Node<T>> TraverseWithParent<T>(this T item, Func<T, IEnumerable<T>> children, int curLevel, T parent)
        {
            yield return new Node<T> { Data = item, Parent = parent, Level = curLevel };
            var items = children(item);
            if (items != null)
            {
                foreach (var n in children(item))
                {
                    foreach (var c in n.TraverseWithParent(children, curLevel + 1, item))
                        yield return c;
                }
            }
        }
        public static IEnumerable<Node<T>> TraverseWithParent<T>(this T item, Func<T, IEnumerable<T>> children)
        {
            return item.TraverseWithParent(children, 0, default(T));
        }

        private static IEnumerable<Node<T>> TraverseListWithParent<T>(this IEnumerable<T> list, Func<T, IEnumerable<T>> children, int curLevel, T parent)
        {
            if (list != null)
            {
                foreach (var n in list)
                {
                    yield return new Node<T> { Data = n, Level = curLevel, Parent = parent };
                    foreach (var m in children(n).TraverseListWithParent(children, curLevel + 1, n))
                        yield return m;
                }
            }
        }
        public static IEnumerable<Node<T>> TraverseListWithParent<T>(this IEnumerable<T> list, Func<T, IEnumerable<T>> children)
        {
            return list.TraverseListWithParent(children, 0, default(T));
        }

        public static IEnumerable<KeyValuePair<String,T>> Flatten<T>(this T item, Func<T, IEnumerable<KeyValuePair<String,T>>> children, String rootKey)
        {
            var keyPrefix = !String.IsNullOrEmpty(rootKey) ? rootKey + "." : "";
            var ch = children(item);

            yield return new KeyValuePair<string, T>(rootKey, item);
            if (ch != null)
            {
                foreach (var e in ch)
                {
                    var cres = Flatten(e.Value, children, keyPrefix + e.Key);
                    foreach (var r in cres)
                        yield return r;
                }
            }
        }
        public static IEnumerable<JToken> Flatten(this JObject obj)
        {
            foreach (var n in obj)
            {
                if (n.Value is JObject)
                {
                    foreach (var nn in Flatten((JObject)n.Value))
                        yield return nn;
                }
                else
                    yield return n.Value;
            }
        }
        public static IEnumerable<T> FindPath<T>(this T item, Func<T, bool> match, Func<T, IEnumerable<T>> children)
        {
            if (match(item))
                yield return item;
            else
            {
                var ch = children(item);
                if (ch != null)
                {
                    foreach (var c in children(item))
                    {
                        var cres = FindPath(c, match, children);
                        if (cres.Any())
                        {
                            yield return item;
                            foreach (var r in cres)
                                yield return r;
                        }
                    }
                }
            }
        }
        public static IEnumerable<T> Enumerate<T>(this T first, params T[] parms)
        {
            IEnumerable<T> First()
            {
                yield return first;
            }
            return First().Concat(parms);
        }
        public static IEnumerable<T> Enumerate<T>(this T first, IEnumerable<T> parms)
        {
            IEnumerable<T> First()
            {
                yield return first;
            }
            if (parms != null)
                return First().Concat(parms);
            else
                return First();
        }
        public static IEnumerable<T> MakeList<T>(params T[] parms)
        {
            foreach (var n in parms)
                if (n != null)
                    yield return n;
        }
    }
}
