﻿namespace QuickType
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using J = Newtonsoft.Json.JsonPropertyAttribute;
    using R = Newtonsoft.Json.Required;
    using N = Newtonsoft.Json.NullValueHandling;
    using System.Threading.Tasks;
    //using Nanolink.Helpers; 
    using System.Net.Http;
    using System.IO;

    public partial class ApiDefinition
    {
        [J("data")] public Data Data { get; set; }
    }

    public partial class Data
    {
        [J("__schema")] public Schema Schema { get; set; }
    }

    public partial class Schema
    {
        [J("queryType")] public MutationTypeClass QueryType { get; set; }
        [J("mutationType")] public MutationTypeClass MutationType { get; set; }
        [J("subscriptionType")] public object SubscriptionType { get; set; }
        [J("types")] public SchemaType[] Types { get; set; }
        [J("directives")] public Directive[] Directives { get; set; }
    }

    public partial class Directive
    {
        [J("name")] public string Name { get; set; }
        [J("description")] public string Description { get; set; }
        [J("locations")] public string[] Locations { get; set; }
        [J("args")] public Arg[] Args { get; set; }
    }

    public partial class Arg
    {
        [J("name")] public string Name { get; set; }
        [J("description")] public string Description { get; set; }
        [J("type")] public PossibleTypeElement Type { get; set; }
        [J("defaultValue")] public DefaultValueUnion DefaultValue { get; set; }
    }

    public partial class PossibleTypeElement
    {
        [J("kind")] public Kind Kind { get; set; }
        [J("name")] public string Name { get; set; }
        [J("ofType")] public PossibleTypeElement OfType { get; set; }
    }

    public partial class MutationTypeClass
    {
        [J("name")] public string Name { get; set; }
    }

    public partial class SchemaType
    {
        [J("kind")] public Kind Kind { get; set; }
        [J("name")] public string Name { get; set; }
        [J("description")] public string Description { get; set; }
        [J("fields")] public Field[] Fields { get; set; }
        [J("inputFields")] public Arg[] InputFields { get; set; }
        [J("interfaces")] public object[] Interfaces { get; set; }
        [J("enumValues")] public EnumValue[] EnumValues { get; set; }
        [J("possibleTypes")] public PossibleTypeElement[] PossibleTypes { get; set; }
    }

    public partial class EnumValue
    {
        [J("name")] public string Name { get; set; }
        [J("description")] public string Description { get; set; }
        [J("isDeprecated")] public bool IsDeprecated { get; set; }
        [J("deprecationReason")] public object DeprecationReason { get; set; }
    }

    public partial class Field
    {
        [J("name")] public string Name { get; set; }
        [J("description")] public string Description { get; set; }
        [J("args")] public Arg[] Args { get; set; }
        [J("type")] public PossibleTypeElement Type { get; set; }
        [J("isDeprecated")] public bool IsDeprecated { get; set; }
        [J("deprecationReason")] public string DeprecationReason { get; set; }
    }

    public enum DefaultValueEnum { NoLongerSupported, Null };

    public enum Kind { Enum, InputObject, List, NonNull, Object, Scalar, Union };

    public partial struct DefaultValueUnion
    {
        public bool? Bool;
        public DefaultValueEnum? Enum;

        public static implicit operator DefaultValueUnion(bool Bool) => new DefaultValueUnion { Bool = Bool };
        public static implicit operator DefaultValueUnion(DefaultValueEnum Enum) => new DefaultValueUnion { Enum = Enum };
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                DefaultValueUnionConverter.Singleton,
                DefaultValueEnumConverter.Singleton,
                KindConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class DefaultValueUnionConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(DefaultValueUnion) || t == typeof(DefaultValueUnion?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.String:
                case JsonToken.Date:
                    var stringValue = serializer.Deserialize<string>(reader);
                    bool b;
                    if (Boolean.TryParse(stringValue, out b))
                    {
                        return new DefaultValueUnion { Bool = b };
                    }
                    switch (stringValue)
                    {
                        case "\"No longer supported\"":
                            return new DefaultValueUnion { Enum = DefaultValueEnum.NoLongerSupported };
                        case "null":
                            return new DefaultValueUnion { Enum = DefaultValueEnum.Null };
                    }
                    break;
            }
            throw new Exception("Cannot unmarshal type DefaultValueUnion");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (DefaultValueUnion)untypedValue;
            if (value.Bool != null)
            {
                var boolString = value.Bool.Value ? "true" : "false";
                serializer.Serialize(writer, boolString);
                return;
            }
            if (value.Enum != null)
            {
                switch (value.Enum)
                {
                    case DefaultValueEnum.NoLongerSupported:
                        serializer.Serialize(writer, "\"No longer supported\"");
                        return;
                    case DefaultValueEnum.Null:
                        serializer.Serialize(writer, "null");
                        return;
                }
            }
            throw new Exception("Cannot marshal type DefaultValueUnion");
        }

        public static readonly DefaultValueUnionConverter Singleton = new DefaultValueUnionConverter();
    }

    internal class DefaultValueEnumConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(DefaultValueEnum) || t == typeof(DefaultValueEnum?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "\"No longer supported\"":
                    return DefaultValueEnum.NoLongerSupported;
                case "null":
                    return DefaultValueEnum.Null;
            }
            throw new Exception("Cannot unmarshal type DefaultValueEnum");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (DefaultValueEnum)untypedValue;
            switch (value)
            {
                case DefaultValueEnum.NoLongerSupported:
                    serializer.Serialize(writer, "\"No longer supported\"");
                    return;
                case DefaultValueEnum.Null:
                    serializer.Serialize(writer, "null");
                    return;
            }
            throw new Exception("Cannot marshal type DefaultValueEnum");
        }

        public static readonly DefaultValueEnumConverter Singleton = new DefaultValueEnumConverter();
    }

    internal class KindConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Kind) || t == typeof(Kind?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "ENUM":
                    return Kind.Enum;
                case "INPUT_OBJECT":
                    return Kind.InputObject;
                case "LIST":
                    return Kind.List;
                case "NON_NULL":
                    return Kind.NonNull;
                case "OBJECT":
                    return Kind.Object;
                case "SCALAR":
                    return Kind.Scalar;
                case "UNION":
                    return Kind.Union;
            }
            throw new Exception("Cannot unmarshal type Kind");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Kind)untypedValue;
            switch (value)
            {
                case Kind.Enum:
                    serializer.Serialize(writer, "ENUM");
                    return;
                case Kind.InputObject:
                    serializer.Serialize(writer, "INPUT_OBJECT");
                    return;
                case Kind.List:
                    serializer.Serialize(writer, "LIST");
                    return;
                case Kind.NonNull:
                    serializer.Serialize(writer, "NON_NULL");
                    return;
                case Kind.Object:
                    serializer.Serialize(writer, "OBJECT");
                    return;
                case Kind.Scalar:
                    serializer.Serialize(writer, "SCALAR");
                    return;
                case Kind.Union:
                    serializer.Serialize(writer, "UNION");
                    return;
            }
            throw new Exception("Cannot marshal type Kind");
        }

        public static readonly KindConverter Singleton = new KindConverter();
    }

    public static class GraphQL
    {
        private const string introSpectionQuery =
        @"{""query"":""\n query IntrospectionQuery {\n __schema {\n queryType { name }\n mutationType { name }\n subscriptionType { name }\n types {\n...FullType\n      }\n directives {\n name\n description\n locations\n args {\n...InputValue\n        }\n      }\n }\n
    }\n\n fragment FullType on __Type {\n kind\n name\n description\n fields(includeDeprecated: true) {\n name\n description\n args {\n...InputValue\n      }\n type {\n...TypeRef\n      }\n isDeprecated\n deprecationReason\n    }\n inputFields {\n      ...InputValue\n }\n interfaces {\n      ...TypeRef\n }\n enumValues(includeDeprecated: true) {\n name\n description\n isDeprecated\n deprecationReason\n    }\n possibleTypes {\n      ...TypeRef\n }\n
}\n\n fragment InputValue on __InputValue {\n name\n description\n type { ...TypeRef }\n defaultValue\n  }\n\n fragment TypeRef on __Type {\n kind\n name\n ofType {\n kind\n name\n ofType {\n kind\n name\n ofType {\n kind\n name\n ofType {\n kind\n name\n ofType {\n kind\n name\n ofType {\n kind\n name\n ofType {\n kind\n name\n                }\n              }\n            }\n          }\n        }\n      }\n }\n  }\n""}
        ";


        public static async Task<ApiDefinition> GetApi(string url)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response;

            try
            {
                response = await client.PostAsync(url, new StringContent(introSpectionQuery));
            }
            catch (Exception e)
            {
                throw e;
            }


            string json;
            using (var reader = new StreamReader(await response.Content.ReadAsStreamAsync()))
            {
                json = reader.ReadToEnd();
            }
            return JsonConvert.DeserializeObject<ApiDefinition>(json, Converter.Settings);
        }
    }
}
