﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using QuickType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using nanolink.shared.Tools;
using System.Globalization;

namespace nanolink.graphql.autogen
{

    public class Generator
    {
        public Schema Schema { get; private set; }
        public string GeneratorNameSpace { get; set; }

        public SchemaType Queries => Schema.Types.FirstOrDefault((s) => s.Name == Schema.QueryType.Name);
        public SchemaType Mutations => Schema.Types.FirstOrDefault((s) => s.Name == Schema.MutationType.Name);

        private IEnumerable<SchemaType> UnionTypes => Schema.Types.Where((s) => s.Kind == Kind.Union);
        private IEnumerable<Field> allFields()
        {
            IEnumerable<Field> retVal = Queries?.Fields;
            if (retVal != null)
            {
                if (Mutations?.Fields != null)
                    retVal = retVal.Concat(Mutations.Fields);
            }
            else
            {
                retVal = Mutations?.Fields;
            }
            return retVal;
        }

        public Generator(ApiDefinition def)
        {
            Schema = def.Data.Schema;
        }

        void generateStart(StringBuilder b, IEnumerable<string> forcedUsings = null, params string[] additionalUsings)
        {
            b.AppendLine("using System;");
            b.AppendLine("using System.Collections.Generic;");
            b.AppendLine("using MongoDB.Bson;");
            if (forcedUsings != null)
            {
                foreach (var u in forcedUsings)
                {
                    b.Append("using ");
                    b.Append(u);
                    b.AppendLine(";");
                }
            }
            foreach (var u in additionalUsings)
            {
                b.Append("using ");
                b.Append(u);
                b.AppendLine(";");
            }
            b.AppendLine($"namespace {GeneratorNameSpace ?? "Nanolink.Helpers.GraphQL.ResultModels"}");
            b.AppendLine("{");
        }
        void generateEnd(StringBuilder b)
        {
            b.AppendLine("}");
        }
        string translateType(string t, Kind kind)
        {
            if (kind == Kind.Scalar)
            {
                if (t == "Date")
                    return "DateTime";
                else
                if (t == "Boolean")
                    return "bool";
                if (t == "Float")
                    return "double";
                else
                if (t != "ObjectId" && t != "DateTime" && t != "DateTimeOffset")
                    return t.ToLower();
            }
            if (kind == Kind.Union)
                return $"Q{t.Substring(1)}";
            return t;
        }
        string generateTypeName(PossibleTypeElement t, Func<string, string> mapTypeName, string postFix = null)
        {
            StringBuilder b = new StringBuilder();
            generateTypeName(b, t, mapTypeName, postFix);
            return b.ToString();
        }
        void generateTypeName(StringBuilder b, PossibleTypeElement t, Func<string, string> mapTypeName, string postFix = null, bool isNullable = true)
        {
            string name = null;
            if (t.Name == null || (name = mapTypeName(t.Name)) == null)
                name = t.Name;
            switch (t.Kind)
            {
                case Kind.Scalar:
                    b.Append(translateType(name, Kind.Scalar));
                    break;
                case Kind.Object:
                    if (t.Name.StartsWith("KV"))
                    {
                        var kvt = Schema.Types.FirstOrDefault(n => n.Name == t.Name);
                        var keytype = kvt.Fields[0].Type;
                        var valtype = kvt.Fields[1].Type;
                        b.Append("KeyValuePair<");
                        generateTypeName(b, keytype, mapTypeName, postFix);
                        b.Append(",");
                        generateTypeName(b, valtype, mapTypeName, postFix);
                        b.Append(">");
                    }
                    else
                    {
                        b.Append(name);
                        if (postFix != null)
                            b.Append(postFix);
                    }
                    break;
                case Kind.InputObject:
                case Kind.Enum:
                    b.Append(name);
                    break;
                case Kind.List:
                    b.Append("List<");
                    generateTypeName(b, t.OfType, mapTypeName, postFix);
                    b.Append(">");
                    break;
                case Kind.NonNull:
                    generateTypeName(b, t.OfType, mapTypeName, postFix, false);
                    break;
                case Kind.Union:
                    if (name.StartsWith("U"))
                        b.Append($"Q{name.Substring(1)}");
                    if (postFix != null)
                        b.Append(postFix);
                    break;
            }
            if (isNullable && (t.Kind == Kind.Enum || t.Kind == Kind.Scalar) && t.Name != "String")
                b.Append("?");
        }

        public IEnumerable<KeyValuePair<string, string>> GenerateApi(IEnumerable<string> additionalNameSpaces, Func<string, string> mapTypeName)
        {
            var apiTypes = (from n in allFields() group n by n.Name.Split("_")[0] into g select g);

            bool isMutation(Field f)
            {
                return Mutations.Fields.FirstOrDefault(n => n.Name == f.Name) != null;
            }

            IEnumerable<string> args(Field f)
            {
                IEnumerable<string> orderedArgs(IEnumerable<Arg> args)
                {
                    var tokenArg = args.Where(n => n.Name == "token").FirstOrDefault();
                    if (tokenArg != null)
                        yield return $"{generateTypeName(tokenArg.Type, mapTypeName)} {tokenArg.Name}";
                    foreach (var n in args.Where(m => m.Type.Kind == Kind.NonNull && m.Name != "token"))
                    {
                        yield return $"{generateTypeName(n.Type, mapTypeName)} {n.Name}";
                    }
                    foreach (var n in args.Where(m => m.Type.Kind != Kind.NonNull && m.Name != "token"))
                    {
                        yield return $"{generateTypeName(n.Type, mapTypeName)} {n.Name} = default";
                    }
                }
                return $"this GraphQLClient con".Enumerate().Concat(orderedArgs(f.Args.Where(n => n.Name != "bag")));
            }

            bool generateArgsMethod(StringBuilder b, Field f)
            {
                bool needsCulture(Arg arg)
                {
                    PossibleTypeElement t = arg.Type;
                    if (arg.Type.Kind == Kind.NonNull)
                    {
                        t = arg.Type.OfType;
                    }
                    if (t.Kind != Kind.Scalar || t.Name == "String")
                        return false;
                    if (t.Name == "Float") // || t.Name == "Date" || t.Name == "DateTime")
                    {
                        return true;
                    }
                    return false;
                }

                if (f.Args.Where((n) => n.Name != "bag").Count() > 0)
                {
                    b.Append("            IEnumerable<string> ");
                    b.AppendLine("genargs()");
                    b.AppendLine("            {");
                    foreach (var n in f.Args)
                    {
                        if (n.Name != "bag")
                        {
                            PossibleTypeElement tp = n.Type;
                            b.Append("                ");
                            if (n.Type.Kind != Kind.NonNull)
                            {
                                b.Append("if (");
                                b.Append(n.Name);
                                if (n.Type.Kind == Kind.Scalar && n.Type.Name != "String")
                                {
                                    b.Append(".HasValue");
                                }
                                else
                                    b.Append(" != null");
                                b.Append(") ");
                            }
                            else
                                tp = n.Type.OfType;

                            if (tp.Name == "ObjectId")
                            {
                                b.Append("yield return $\"");
                                b.Append(n.Name);
                                b.Append(": \\\"{");
                                b.Append(n.Name);
                                b.Append(".ToString()");
                                b.AppendLine("}\\\"\";");
                            }
                            else
                            {
                                bool nc = needsCulture(n);
                                b.Append("yield return ");
                                if (nc)
                                    b.Append("FormattableString.Invariant($\"");
                                else
                                    b.Append("$\"");
                                b.Append(n.Name);
                                if (tp.Name == "Date" || tp.Name == "DateTime")
                                    b.Append(": \\\"{");
                                else
                                    b.Append(": {");

                                if (tp.Name == "String")
                                {
                                    b.Append("JsonConvert.ToString(");
                                    b.Append(n.Name);
                                    b.Append(",'\\\"', StringEscapeHandling.Default)");
                                }
                                else
                                if (tp.Kind == Kind.InputObject || tp.Kind == Kind.List)
                                {
                                    b.Append(n.Name);
                                    b.Append(".ToGraphQLJson()");
                                }
                                else
                                {
                                    b.Append(n.Name);
                                    if (n.Type.Kind == Kind.Scalar && n.Type.Name != "String")
                                        b.Append(".Value");
                                    if (tp.Name == "Boolean")
                                    {
                                        b.Append(".ToString().ToLower()");
                                    }
                                    else
                                    if (tp.Name == "Date" || tp.Name == "DateTime")
                                        b.Append(":o");
                                }
                                b.Append("}");
                                if (tp.Name == "Date" || tp.Name == "DateTime")
                                    b.Append("\\\"");

                                if (nc)
                                    b.AppendLine("\");");
                                else
                                    b.AppendLine("\";");
                            }
                        }
                    }
                    b.AppendLine("            }");
                    return true;
                }
                return false;
            }

            void generateMethod(StringBuilder b, Field f)
            {
                var typ = Schema.Types.First((t) => t.Name == f.Type.Name);
                var rfld = typ.Fields.FirstOrDefault((n) => n.Name == "result");
                string resultType = null;
                string returnType = null;
                bool objectReturnType = false;

                b.Append("        public static async Task<");
                if (rfld.Type.Kind == Kind.List)
                {
                    resultType = generateTypeName(rfld.Type.OfType, mapTypeName);
                    if (rfld.Type.OfType.Kind == Kind.Object || rfld.Type.OfType.Kind == Kind.Union)
                    {
                        objectReturnType = true;
                        if (typ.Fields.FirstOrDefault((fld) => fld.Name == "version") != null)
                        {
                            returnType = "ApiResultListWithVersion<T>";
                        }
                        else
                        {
                            returnType = "ApiResultList<T>";
                        }
                    }
                    else
                    {
                        if (typ.Fields.FirstOrDefault((fld) => fld.Name == "version") != null)
                        {
                            returnType = $"ApiResultListWithVersion<{resultType}>";
                        }
                        else
                        {
                            returnType = $"ApiResultList<{resultType}>";
                        }
                    }
                }
                else
                {
                    resultType = generateTypeName(rfld.Type, mapTypeName);
                    if (rfld.Type.Kind == Kind.Object || rfld.Type.Kind == Kind.Union)
                    {
                        returnType = "ApiResult<T>";
                        objectReturnType = true;
                    }
                    else
                    {
                        returnType = $"ApiResult<{resultType}>";
                    }
                }
                b.Append(returnType);
                b.Append("> ");
                b.Append(String.Join('_', (from n in f.Name.Split('_') select n.ToUpperCamelCase())));
                if (objectReturnType)
                    b.Append("<T>(");
                else
                    b.Append("(");
                b.Append(String.Join(',', args(f)));
                b.Append(")");
                if (objectReturnType)
                {
                    b.Append(" where T: ");
                    b.AppendLine(resultType);
                }
                else
                    b.AppendLine();
                b.AppendLine("        {");
                bool hasArgs = generateArgsMethod(b, f);
                if (hasArgs)
                {
                    b.Append("            ");
                    b.Append("string args = string.Join(\",\", ");
                    b.Append("genargs");
                    b.Append("(");
                    b.AppendLine("));");
                }
                b.Append("            ");
                if (objectReturnType)
                    b.Append("return (await con.Post<T>(\"api/public\", ");
                else
                    b.Append($"return (await con.Post<{resultType}>(\"api/public\", ");
                if (isMutation(f))
                    b.Append("true,");
                else
                    b.Append("false, ");
                b.Append(" \"");
                b.Append(f.Name);
                b.Append("\", ");
                if (rfld.Type.Kind == Kind.List)
                    b.Append("true,");
                else
                    b.Append("false,");
                if (hasArgs)
                    b.Append(" args,");
                else
                    b.Append(" null,");
                //if (objectReturnType)
                //    b.Append(" null, typeof(T).ResolveTypeFunc())) as ");
                //else
                //    b.Append($" null, typeof({resultType}).ResolveTypeFunc())) as ");
                b.Append(" null)) as ");
                b.Append(returnType);
                b.AppendLine(";");

                b.AppendLine("        }");
            }

            foreach (var n in apiTypes)
            {
                StringBuilder b = new StringBuilder();
                generateStart(b, additionalNameSpaces,
                    "Newtonsoft.Json",
                    "System.Globalization",
                    "System.Threading.Tasks");
                b.Append("    public static class ");
                b.AppendLine($"{n.Key.ToUpperCamelCase()}Api");
                b.AppendLine("    {");
                foreach (var f in n)
                {
                    generateMethod(b, f);
                    //generateJObjectMethod(b, f, hasargs);
                }
                b.AppendLine("    }");
                generateEnd(b);
                yield return new KeyValuePair<string, string>(n.Key, b.ToString());
            }
        }

        public IEnumerable<KeyValuePair<string, string>> GenerateTypes(Func<string, string> mapTypeName)
        {

            SchemaType baseClass(string tName)
            {
                foreach (var n in UnionTypes)
                {
                    if (n.PossibleTypes.FirstOrDefault((t) => t.Name == tName) != null)
                        return n;
                }
                return null;
            }

            string generateInterface(StringBuilder b, SchemaType t)
            {
                var bClass = baseClass(t.Name);
                var baseFields = baseClassFields(bClass, t)?.ToHashSet();
                b.Append("    public abstract class ");
                b.Append(t.Name);
                if (bClass != null)
                {
                    b.Append(": ");
                    b.Append($"Q{bClass.Name.Substring(1)}");
                }
                b.AppendLine();
                b.AppendLine("    {");

                foreach (var n in t.Fields)
                {
                    if (baseFields == null || !baseFields.Contains(n.Name))
                    {
                        b.Append("        ");
                        b.Append("public virtual ");
                        if (t.Name == "QBinaryBlob" && n.Name == "dataBase64")
                        {
                            generateTypeName(b, n.Type, mapTypeName, null);
                            b.Append(" ");
                            b.Append(n.Name.ToUpperCamelCase());
                            b.AppendLine();
                            b.AppendLine("        {");
                            b.AppendLine("            get { return Convert.ToBase64String(Data); }");
                            b.AppendLine("            set");
                            b.AppendLine("            {");
                            b.AppendLine("                Data = value != null ? Data = Convert.FromBase64String(value) : null;");
                            b.AppendLine("            }");
                            b.AppendLine("        }");
                        }
                        else
                        {
                            generateTypeName(b, n.Type, mapTypeName, null);
                            b.Append(" ");
                            b.Append(n.Name.ToUpperCamelCase());
                            b.AppendLine(" { get => default; set {} }");
                        }
                    }
                }
                if (t.Name == "QBinaryBlob")
                {
                    b.AppendLine("        [JsonIgnore]");
                    b.AppendLine("        [System.Text.Json.Serialization.JsonIgnore]");
                    b.AppendLine("        public byte[] Data { get; set; }");
                }
                if (bClass == null)
                    b.AppendLine("        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });");
                b.AppendLine("    }");
                return t.Name;
            }

            string generateInputClass(StringBuilder b, SchemaType t)
            {
                var bClass = baseClass(t.Name);
                var baseFields = baseClassFields(bClass, t)?.ToHashSet();
                b.Append("    public class ");
                b.Append(t.Name);
                if (bClass != null)
                {
                    b.Append(": ");
                    b.Append($"Q{bClass.Name.Substring(1)}");
                }
                b.AppendLine("");
                b.AppendLine("    {");

                foreach (var n in t.InputFields)
                {
                    if (baseFields == null || !baseFields.Contains(n.Name))
                    {
                        b.Append("        public ");
                        generateTypeName(b, n.Type, mapTypeName);
                        b.Append(" ");
                        b.Append(n.Name.ToUpperCamelCase());
                        b.AppendLine(" { get; set; }");
                    }
                }
                b.AppendLine("    }");
                return t.Name;
            }

            IEnumerable<string> baseClassFields(SchemaType baseType, SchemaType firstImplementor)
            {
                if (baseType == null)
                    return null;
                if (baseType.PossibleTypes.Length > 1)
                {
                    var t2name = baseType.PossibleTypes.FirstOrDefault((n) => n.Name != firstImplementor.Name).Name;
                    var t2 = Schema.Types.FirstOrDefault((n) => n.Name == t2name);
                    return (from n in firstImplementor.Fields select n.Name).Intersect(
                            (from n in t2.Fields select n.Name)
                        );
                }
                return null;
            }
            string generateEnum(StringBuilder b, SchemaType t)
            {
                b.Append("    public enum ");
                b.AppendLine(t.Name);
                b.AppendLine("    {");
                b.Append("        ");
                b.AppendLine(String.Join(",\r\n        ", (from n in t.EnumValues select n.Name)));
                b.AppendLine("    }");
                return t.Name;
            }
            string generateUnion(StringBuilder b, SchemaType t)
            {
                IEnumerable<Field> fields = null;
                if (t.PossibleTypes.Length > 1)
                {
                    var t1 = Schema.Types.FirstOrDefault((n) => n.Name == t.PossibleTypes.First().Name);
                    var baseFields = baseClassFields(t, t1).ToHashSet();
                    fields = t1.Fields.Where((n) => baseFields.Contains(n.Name));
                }
                b.Append("    [Polymorph(");
                b.Append(String.Join(",", from n in t.PossibleTypes select $"typeof({n.Name})"));
                b.AppendLine(")]");
                b.Append("    public abstract class ");
                b.Append($"Q{t.Name.Substring(1)}");
                b.AppendLine();
                b.AppendLine("    {");
                if (fields != null)
                {
                    foreach (var n in fields)
                    {
                        b.Append("        public virtual ");
                        generateTypeName(b, n.Type, mapTypeName);
                        b.Append(" ");
                        b.Append(n.Name.ToUpperCamelCase());
                        b.AppendLine(" { get => default; set {} }");
                    }
                }
                b.AppendLine("        public string ToJson() => Newtonsoft.Json.JsonConvert.SerializeObject(this, new Newtonsoft.Json.JsonSerializerSettings { ContractResolver = new GraphQLNewtonsoftContractResolver() });");
                b.AppendLine("    }");
                return $"Q{t.Name.Substring(1)}";
            }

            foreach (var n in Schema.Types)
            {
                StringBuilder b = new StringBuilder();
                if (n.Name != Schema.MutationType.Name &&
                    n.Name != Schema.QueryType.Name &&
                    n.Kind != Kind.Scalar && !n.Name.StartsWith("__")
                    && mapTypeName(n.Name) == null)
                {
                    if (n.Kind == Kind.Object)
                    {
                        if (n.Name.StartsWith("Q"))
                        {
                            generateStart(b, null, "Newtonsoft.Json");
                            var name = generateInterface(b, n);
                            generateEnd(b);
                            yield return new KeyValuePair<string, string>(name, b.ToString());
                        }
                    }
                    else
                    if (n.Kind == Kind.Union)
                    {
                        generateStart(b);
                        var name = generateUnion(b, n);
                        generateEnd(b);
                        yield return new KeyValuePair<string, string>(name, b.ToString());
                    }
                    else
                    if (n.Kind == Kind.Enum)
                    {
                        generateStart(b);
                        var name = generateEnum(b, n);
                        generateEnd(b);
                        yield return new KeyValuePair<string, string>(name, b.ToString());
                    }
                    else
                    if (n.Kind == Kind.InputObject)
                    {
                        generateStart(b);
                        var name = generateInputClass(b, n);
                        generateEnd(b);
                        yield return new KeyValuePair<string, string>(name, b.ToString());
                    }
                }
            }
        }
    }
}