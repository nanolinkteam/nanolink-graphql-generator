﻿using nanolink.graphql.autogen;
using nanolink.shared.Tools;
using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace nanolink.graphql.generator
{
    class Program
    {
        private static string OutputPath([CallerFilePath] string fp = null)
        {
            var rel = Path.GetRelativePath(ProgramPath, Path.GetDirectoryName(fp));
            return $"{rel}\\..\\nanolink.graphql.api\\api\\";
        }
        static string ProgramPath => AppDomain.CurrentDomain.BaseDirectory;
        static readonly string ModelPath = Path.Combine(OutputPath(), "Model");
        static readonly string ApiPath = Path.Combine(OutputPath(), "Api");

        private static void CleanUp()
        {
            if (Directory.Exists(ModelPath))
            {
                foreach (var n in Directory.GetFiles(ModelPath))
                {
                    if (Path.GetFileName(n) != "ResolvePathAttribute.cs"
                        && Path.GetFileName(n) != "QResultExtensions.cs")
                    {
                        File.Delete(n);
                    }
                }
            }
            else
                Directory.CreateDirectory(ModelPath);
            if (Directory.Exists(ApiPath))
            {
                foreach (var n in Directory.GetFiles(ApiPath))
                {
                    File.Delete(n);
                }
            }
            else
                Directory.CreateDirectory(ApiPath);
        }


        static async Task generate(string url)
        {
            var def = await QuickType.GraphQL.GetApi(url);
            var gen = new Generator(def) { GeneratorNameSpace = "nanolink.graphql.api" };
            CleanUp();

            foreach (var n in gen.GenerateTypes((_) => null))
            {
                var fname = Path.Combine(ModelPath, $"{n.Key}.cs");
                File.WriteAllText(fname, n.Value);
            }
            foreach (var n in gen.GenerateApi(null, (_) => null))
            {
                var fname = Path.Combine(ApiPath, $"{n.Key}.cs");
                File.WriteAllText(fname, n.Value);
            }

        }
        static void Main(string[] args)
        {
            string url = "https://dk1.nanolink.com/api/public";
            ManualResetEvent ev = new ManualResetEvent(false);
            Task.Run(() =>
            {
                generate(url).ContinueWith((t) => ev.Set());
            });
            ev.WaitOne();
        }
    }
}
